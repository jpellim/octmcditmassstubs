/**
 * DefaultServiceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.components.webservice.beans.xsd;

public class DefaultServiceResponse  implements java.io.Serializable {
    private java.lang.String[] errors;

    private java.lang.String[] notes;

    private java.lang.String resourceName;

    private com.inteqnet.components.webservice.beans.xsd.Bean responseBean;

    private java.lang.String responseFormat;

    private java.lang.String responseStatus;

    private java.lang.String responseText;

    private java.lang.String statusCode;

    private java.lang.String statusMessage;

    private java.lang.String[] warnings;

    public DefaultServiceResponse() {
    }

    public DefaultServiceResponse(
           java.lang.String[] errors,
           java.lang.String[] notes,
           java.lang.String resourceName,
           com.inteqnet.components.webservice.beans.xsd.Bean responseBean,
           java.lang.String responseFormat,
           java.lang.String responseStatus,
           java.lang.String responseText,
           java.lang.String statusCode,
           java.lang.String statusMessage,
           java.lang.String[] warnings) {
           this.errors = errors;
           this.notes = notes;
           this.resourceName = resourceName;
           this.responseBean = responseBean;
           this.responseFormat = responseFormat;
           this.responseStatus = responseStatus;
           this.responseText = responseText;
           this.statusCode = statusCode;
           this.statusMessage = statusMessage;
           this.warnings = warnings;
    }


    /**
     * Gets the errors value for this DefaultServiceResponse.
     * 
     * @return errors
     */
    public java.lang.String[] getErrors() {
        return errors;
    }


    /**
     * Sets the errors value for this DefaultServiceResponse.
     * 
     * @param errors
     */
    public void setErrors(java.lang.String[] errors) {
        this.errors = errors;
    }

    public java.lang.String getErrors(int i) {
        return this.errors[i];
    }

    public void setErrors(int i, java.lang.String _value) {
        this.errors[i] = _value;
    }


    /**
     * Gets the notes value for this DefaultServiceResponse.
     * 
     * @return notes
     */
    public java.lang.String[] getNotes() {
        return notes;
    }


    /**
     * Sets the notes value for this DefaultServiceResponse.
     * 
     * @param notes
     */
    public void setNotes(java.lang.String[] notes) {
        this.notes = notes;
    }

    public java.lang.String getNotes(int i) {
        return this.notes[i];
    }

    public void setNotes(int i, java.lang.String _value) {
        this.notes[i] = _value;
    }


    /**
     * Gets the resourceName value for this DefaultServiceResponse.
     * 
     * @return resourceName
     */
    public java.lang.String getResourceName() {
        return resourceName;
    }


    /**
     * Sets the resourceName value for this DefaultServiceResponse.
     * 
     * @param resourceName
     */
    public void setResourceName(java.lang.String resourceName) {
        this.resourceName = resourceName;
    }


    /**
     * Gets the responseBean value for this DefaultServiceResponse.
     * 
     * @return responseBean
     */
    public com.inteqnet.components.webservice.beans.xsd.Bean getResponseBean() {
        return responseBean;
    }


    /**
     * Sets the responseBean value for this DefaultServiceResponse.
     * 
     * @param responseBean
     */
    public void setResponseBean(com.inteqnet.components.webservice.beans.xsd.Bean responseBean) {
        this.responseBean = responseBean;
    }


    /**
     * Gets the responseFormat value for this DefaultServiceResponse.
     * 
     * @return responseFormat
     */
    public java.lang.String getResponseFormat() {
        return responseFormat;
    }


    /**
     * Sets the responseFormat value for this DefaultServiceResponse.
     * 
     * @param responseFormat
     */
    public void setResponseFormat(java.lang.String responseFormat) {
        this.responseFormat = responseFormat;
    }


    /**
     * Gets the responseStatus value for this DefaultServiceResponse.
     * 
     * @return responseStatus
     */
    public java.lang.String getResponseStatus() {
        return responseStatus;
    }


    /**
     * Sets the responseStatus value for this DefaultServiceResponse.
     * 
     * @param responseStatus
     */
    public void setResponseStatus(java.lang.String responseStatus) {
        this.responseStatus = responseStatus;
    }


    /**
     * Gets the responseText value for this DefaultServiceResponse.
     * 
     * @return responseText
     */
    public java.lang.String getResponseText() {
        return responseText;
    }


    /**
     * Sets the responseText value for this DefaultServiceResponse.
     * 
     * @param responseText
     */
    public void setResponseText(java.lang.String responseText) {
        this.responseText = responseText;
    }


    /**
     * Gets the statusCode value for this DefaultServiceResponse.
     * 
     * @return statusCode
     */
    public java.lang.String getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this DefaultServiceResponse.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the statusMessage value for this DefaultServiceResponse.
     * 
     * @return statusMessage
     */
    public java.lang.String getStatusMessage() {
        return statusMessage;
    }


    /**
     * Sets the statusMessage value for this DefaultServiceResponse.
     * 
     * @param statusMessage
     */
    public void setStatusMessage(java.lang.String statusMessage) {
        this.statusMessage = statusMessage;
    }


    /**
     * Gets the warnings value for this DefaultServiceResponse.
     * 
     * @return warnings
     */
    public java.lang.String[] getWarnings() {
        return warnings;
    }


    /**
     * Sets the warnings value for this DefaultServiceResponse.
     * 
     * @param warnings
     */
    public void setWarnings(java.lang.String[] warnings) {
        this.warnings = warnings;
    }

    public java.lang.String getWarnings(int i) {
        return this.warnings[i];
    }

    public void setWarnings(int i, java.lang.String _value) {
        this.warnings[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DefaultServiceResponse)) return false;
        DefaultServiceResponse other = (DefaultServiceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errors==null && other.getErrors()==null) || 
             (this.errors!=null &&
              java.util.Arrays.equals(this.errors, other.getErrors()))) &&
            ((this.notes==null && other.getNotes()==null) || 
             (this.notes!=null &&
              java.util.Arrays.equals(this.notes, other.getNotes()))) &&
            ((this.resourceName==null && other.getResourceName()==null) || 
             (this.resourceName!=null &&
              this.resourceName.equals(other.getResourceName()))) &&
            ((this.responseBean==null && other.getResponseBean()==null) || 
             (this.responseBean!=null &&
              this.responseBean.equals(other.getResponseBean()))) &&
            ((this.responseFormat==null && other.getResponseFormat()==null) || 
             (this.responseFormat!=null &&
              this.responseFormat.equals(other.getResponseFormat()))) &&
            ((this.responseStatus==null && other.getResponseStatus()==null) || 
             (this.responseStatus!=null &&
              this.responseStatus.equals(other.getResponseStatus()))) &&
            ((this.responseText==null && other.getResponseText()==null) || 
             (this.responseText!=null &&
              this.responseText.equals(other.getResponseText()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.statusMessage==null && other.getStatusMessage()==null) || 
             (this.statusMessage!=null &&
              this.statusMessage.equals(other.getStatusMessage()))) &&
            ((this.warnings==null && other.getWarnings()==null) || 
             (this.warnings!=null &&
              java.util.Arrays.equals(this.warnings, other.getWarnings())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrors() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErrors());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErrors(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNotes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNotes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNotes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResourceName() != null) {
            _hashCode += getResourceName().hashCode();
        }
        if (getResponseBean() != null) {
            _hashCode += getResponseBean().hashCode();
        }
        if (getResponseFormat() != null) {
            _hashCode += getResponseFormat().hashCode();
        }
        if (getResponseStatus() != null) {
            _hashCode += getResponseStatus().hashCode();
        }
        if (getResponseText() != null) {
            _hashCode += getResponseText().hashCode();
        }
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getStatusMessage() != null) {
            _hashCode += getStatusMessage().hashCode();
        }
        if (getWarnings() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWarnings());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWarnings(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DefaultServiceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errors");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "errors"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "notes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resourceName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "resourceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseBean");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "responseBean"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "Bean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "responseFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "responseStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "responseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "statusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "statusMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("warnings");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "warnings"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
