/**
 * Credentials.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.beans.xsd;

public class Credentials  implements java.io.Serializable {
    private java.lang.String authorizationToken;

    private java.lang.String sliceToken;

    private java.lang.String userName;

    private java.lang.String userPassword;

    public Credentials() {
    }

    public Credentials(
           java.lang.String authorizationToken,
           java.lang.String sliceToken,
           java.lang.String userName,
           java.lang.String userPassword) {
           this.authorizationToken = authorizationToken;
           this.sliceToken = sliceToken;
           this.userName = userName;
           this.userPassword = userPassword;
    }


    /**
     * Gets the authorizationToken value for this Credentials.
     * 
     * @return authorizationToken
     */
    public java.lang.String getAuthorizationToken() {
        return authorizationToken;
    }


    /**
     * Sets the authorizationToken value for this Credentials.
     * 
     * @param authorizationToken
     */
    public void setAuthorizationToken(java.lang.String authorizationToken) {
        this.authorizationToken = authorizationToken;
    }


    /**
     * Gets the sliceToken value for this Credentials.
     * 
     * @return sliceToken
     */
    public java.lang.String getSliceToken() {
        return sliceToken;
    }


    /**
     * Sets the sliceToken value for this Credentials.
     * 
     * @param sliceToken
     */
    public void setSliceToken(java.lang.String sliceToken) {
        this.sliceToken = sliceToken;
    }


    /**
     * Gets the userName value for this Credentials.
     * 
     * @return userName
     */
    public java.lang.String getUserName() {
        return userName;
    }


    /**
     * Sets the userName value for this Credentials.
     * 
     * @param userName
     */
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }


    /**
     * Gets the userPassword value for this Credentials.
     * 
     * @return userPassword
     */
    public java.lang.String getUserPassword() {
        return userPassword;
    }


    /**
     * Sets the userPassword value for this Credentials.
     * 
     * @param userPassword
     */
    public void setUserPassword(java.lang.String userPassword) {
        this.userPassword = userPassword;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Credentials)) return false;
        Credentials other = (Credentials) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.authorizationToken==null && other.getAuthorizationToken()==null) || 
             (this.authorizationToken!=null &&
              this.authorizationToken.equals(other.getAuthorizationToken()))) &&
            ((this.sliceToken==null && other.getSliceToken()==null) || 
             (this.sliceToken!=null &&
              this.sliceToken.equals(other.getSliceToken()))) &&
            ((this.userName==null && other.getUserName()==null) || 
             (this.userName!=null &&
              this.userName.equals(other.getUserName()))) &&
            ((this.userPassword==null && other.getUserPassword()==null) || 
             (this.userPassword!=null &&
              this.userPassword.equals(other.getUserPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthorizationToken() != null) {
            _hashCode += getAuthorizationToken().hashCode();
        }
        if (getSliceToken() != null) {
            _hashCode += getSliceToken().hashCode();
        }
        if (getUserName() != null) {
            _hashCode += getUserName().hashCode();
        }
        if (getUserPassword() != null) {
            _hashCode += getUserPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Credentials.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationToken");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "authorizationToken"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sliceToken");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "sliceToken"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "userName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userPassword");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "userPassword"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
