/**
 * Worklog.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.beans.xsd;

public class Worklog  extends com.inteqnet.components.webservice.beans.xsd.Bean  implements java.io.Serializable {
    private java.lang.Integer item_id;

    private java.lang.Integer row_id;

    private java.lang.String ticket_identifier;

    private java.lang.String ticket_type;

    private java.lang.String work_actual_date;

    private java.lang.String work_created_by;

    private java.lang.Integer work_created_by_contact_id;

    private java.lang.String work_created_date;

    private java.lang.String work_description;

    private java.lang.String work_modified_by;

    private java.lang.Integer work_modified_by_contact_id;

    private java.lang.String work_modified_date;

    private java.lang.Integer work_time_spent;

    private java.lang.String work_type;

    private java.lang.Integer work_type_code;

    private java.lang.String work_view_type;

    public Worklog() {
    }

    public Worklog(
           java.lang.Integer item_id,
           java.lang.Integer row_id,
           java.lang.String ticket_identifier,
           java.lang.String ticket_type,
           java.lang.String work_actual_date,
           java.lang.String work_created_by,
           java.lang.Integer work_created_by_contact_id,
           java.lang.String work_created_date,
           java.lang.String work_description,
           java.lang.String work_modified_by,
           java.lang.Integer work_modified_by_contact_id,
           java.lang.String work_modified_date,
           java.lang.Integer work_time_spent,
           java.lang.String work_type,
           java.lang.Integer work_type_code,
           java.lang.String work_view_type) {
        this.item_id = item_id;
        this.row_id = row_id;
        this.ticket_identifier = ticket_identifier;
        this.ticket_type = ticket_type;
        this.work_actual_date = work_actual_date;
        this.work_created_by = work_created_by;
        this.work_created_by_contact_id = work_created_by_contact_id;
        this.work_created_date = work_created_date;
        this.work_description = work_description;
        this.work_modified_by = work_modified_by;
        this.work_modified_by_contact_id = work_modified_by_contact_id;
        this.work_modified_date = work_modified_date;
        this.work_time_spent = work_time_spent;
        this.work_type = work_type;
        this.work_type_code = work_type_code;
        this.work_view_type = work_view_type;
    }


    /**
     * Gets the item_id value for this Worklog.
     * 
     * @return item_id
     */
    public java.lang.Integer getItem_id() {
        return item_id;
    }


    /**
     * Sets the item_id value for this Worklog.
     * 
     * @param item_id
     */
    public void setItem_id(java.lang.Integer item_id) {
        this.item_id = item_id;
    }


    /**
     * Gets the row_id value for this Worklog.
     * 
     * @return row_id
     */
    public java.lang.Integer getRow_id() {
        return row_id;
    }


    /**
     * Sets the row_id value for this Worklog.
     * 
     * @param row_id
     */
    public void setRow_id(java.lang.Integer row_id) {
        this.row_id = row_id;
    }


    /**
     * Gets the ticket_identifier value for this Worklog.
     * 
     * @return ticket_identifier
     */
    public java.lang.String getTicket_identifier() {
        return ticket_identifier;
    }


    /**
     * Sets the ticket_identifier value for this Worklog.
     * 
     * @param ticket_identifier
     */
    public void setTicket_identifier(java.lang.String ticket_identifier) {
        this.ticket_identifier = ticket_identifier;
    }


    /**
     * Gets the ticket_type value for this Worklog.
     * 
     * @return ticket_type
     */
    public java.lang.String getTicket_type() {
        return ticket_type;
    }


    /**
     * Sets the ticket_type value for this Worklog.
     * 
     * @param ticket_type
     */
    public void setTicket_type(java.lang.String ticket_type) {
        this.ticket_type = ticket_type;
    }


    /**
     * Gets the work_actual_date value for this Worklog.
     * 
     * @return work_actual_date
     */
    public java.lang.String getWork_actual_date() {
        return work_actual_date;
    }


    /**
     * Sets the work_actual_date value for this Worklog.
     * 
     * @param work_actual_date
     */
    public void setWork_actual_date(java.lang.String work_actual_date) {
        this.work_actual_date = work_actual_date;
    }


    /**
     * Gets the work_created_by value for this Worklog.
     * 
     * @return work_created_by
     */
    public java.lang.String getWork_created_by() {
        return work_created_by;
    }


    /**
     * Sets the work_created_by value for this Worklog.
     * 
     * @param work_created_by
     */
    public void setWork_created_by(java.lang.String work_created_by) {
        this.work_created_by = work_created_by;
    }


    /**
     * Gets the work_created_by_contact_id value for this Worklog.
     * 
     * @return work_created_by_contact_id
     */
    public java.lang.Integer getWork_created_by_contact_id() {
        return work_created_by_contact_id;
    }


    /**
     * Sets the work_created_by_contact_id value for this Worklog.
     * 
     * @param work_created_by_contact_id
     */
    public void setWork_created_by_contact_id(java.lang.Integer work_created_by_contact_id) {
        this.work_created_by_contact_id = work_created_by_contact_id;
    }


    /**
     * Gets the work_created_date value for this Worklog.
     * 
     * @return work_created_date
     */
    public java.lang.String getWork_created_date() {
        return work_created_date;
    }


    /**
     * Sets the work_created_date value for this Worklog.
     * 
     * @param work_created_date
     */
    public void setWork_created_date(java.lang.String work_created_date) {
        this.work_created_date = work_created_date;
    }


    /**
     * Gets the work_description value for this Worklog.
     * 
     * @return work_description
     */
    public java.lang.String getWork_description() {
        return work_description;
    }


    /**
     * Sets the work_description value for this Worklog.
     * 
     * @param work_description
     */
    public void setWork_description(java.lang.String work_description) {
        this.work_description = work_description;
    }


    /**
     * Gets the work_modified_by value for this Worklog.
     * 
     * @return work_modified_by
     */
    public java.lang.String getWork_modified_by() {
        return work_modified_by;
    }


    /**
     * Sets the work_modified_by value for this Worklog.
     * 
     * @param work_modified_by
     */
    public void setWork_modified_by(java.lang.String work_modified_by) {
        this.work_modified_by = work_modified_by;
    }


    /**
     * Gets the work_modified_by_contact_id value for this Worklog.
     * 
     * @return work_modified_by_contact_id
     */
    public java.lang.Integer getWork_modified_by_contact_id() {
        return work_modified_by_contact_id;
    }


    /**
     * Sets the work_modified_by_contact_id value for this Worklog.
     * 
     * @param work_modified_by_contact_id
     */
    public void setWork_modified_by_contact_id(java.lang.Integer work_modified_by_contact_id) {
        this.work_modified_by_contact_id = work_modified_by_contact_id;
    }


    /**
     * Gets the work_modified_date value for this Worklog.
     * 
     * @return work_modified_date
     */
    public java.lang.String getWork_modified_date() {
        return work_modified_date;
    }


    /**
     * Sets the work_modified_date value for this Worklog.
     * 
     * @param work_modified_date
     */
    public void setWork_modified_date(java.lang.String work_modified_date) {
        this.work_modified_date = work_modified_date;
    }


    /**
     * Gets the work_time_spent value for this Worklog.
     * 
     * @return work_time_spent
     */
    public java.lang.Integer getWork_time_spent() {
        return work_time_spent;
    }


    /**
     * Sets the work_time_spent value for this Worklog.
     * 
     * @param work_time_spent
     */
    public void setWork_time_spent(java.lang.Integer work_time_spent) {
        this.work_time_spent = work_time_spent;
    }


    /**
     * Gets the work_type value for this Worklog.
     * 
     * @return work_type
     */
    public java.lang.String getWork_type() {
        return work_type;
    }


    /**
     * Sets the work_type value for this Worklog.
     * 
     * @param work_type
     */
    public void setWork_type(java.lang.String work_type) {
        this.work_type = work_type;
    }


    /**
     * Gets the work_type_code value for this Worklog.
     * 
     * @return work_type_code
     */
    public java.lang.Integer getWork_type_code() {
        return work_type_code;
    }


    /**
     * Sets the work_type_code value for this Worklog.
     * 
     * @param work_type_code
     */
    public void setWork_type_code(java.lang.Integer work_type_code) {
        this.work_type_code = work_type_code;
    }


    /**
     * Gets the work_view_type value for this Worklog.
     * 
     * @return work_view_type
     */
    public java.lang.String getWork_view_type() {
        return work_view_type;
    }


    /**
     * Sets the work_view_type value for this Worklog.
     * 
     * @param work_view_type
     */
    public void setWork_view_type(java.lang.String work_view_type) {
        this.work_view_type = work_view_type;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Worklog)) return false;
        Worklog other = (Worklog) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.item_id==null && other.getItem_id()==null) || 
             (this.item_id!=null &&
              this.item_id.equals(other.getItem_id()))) &&
            ((this.row_id==null && other.getRow_id()==null) || 
             (this.row_id!=null &&
              this.row_id.equals(other.getRow_id()))) &&
            ((this.ticket_identifier==null && other.getTicket_identifier()==null) || 
             (this.ticket_identifier!=null &&
              this.ticket_identifier.equals(other.getTicket_identifier()))) &&
            ((this.ticket_type==null && other.getTicket_type()==null) || 
             (this.ticket_type!=null &&
              this.ticket_type.equals(other.getTicket_type()))) &&
            ((this.work_actual_date==null && other.getWork_actual_date()==null) || 
             (this.work_actual_date!=null &&
              this.work_actual_date.equals(other.getWork_actual_date()))) &&
            ((this.work_created_by==null && other.getWork_created_by()==null) || 
             (this.work_created_by!=null &&
              this.work_created_by.equals(other.getWork_created_by()))) &&
            ((this.work_created_by_contact_id==null && other.getWork_created_by_contact_id()==null) || 
             (this.work_created_by_contact_id!=null &&
              this.work_created_by_contact_id.equals(other.getWork_created_by_contact_id()))) &&
            ((this.work_created_date==null && other.getWork_created_date()==null) || 
             (this.work_created_date!=null &&
              this.work_created_date.equals(other.getWork_created_date()))) &&
            ((this.work_description==null && other.getWork_description()==null) || 
             (this.work_description!=null &&
              this.work_description.equals(other.getWork_description()))) &&
            ((this.work_modified_by==null && other.getWork_modified_by()==null) || 
             (this.work_modified_by!=null &&
              this.work_modified_by.equals(other.getWork_modified_by()))) &&
            ((this.work_modified_by_contact_id==null && other.getWork_modified_by_contact_id()==null) || 
             (this.work_modified_by_contact_id!=null &&
              this.work_modified_by_contact_id.equals(other.getWork_modified_by_contact_id()))) &&
            ((this.work_modified_date==null && other.getWork_modified_date()==null) || 
             (this.work_modified_date!=null &&
              this.work_modified_date.equals(other.getWork_modified_date()))) &&
            ((this.work_time_spent==null && other.getWork_time_spent()==null) || 
             (this.work_time_spent!=null &&
              this.work_time_spent.equals(other.getWork_time_spent()))) &&
            ((this.work_type==null && other.getWork_type()==null) || 
             (this.work_type!=null &&
              this.work_type.equals(other.getWork_type()))) &&
            ((this.work_type_code==null && other.getWork_type_code()==null) || 
             (this.work_type_code!=null &&
              this.work_type_code.equals(other.getWork_type_code()))) &&
            ((this.work_view_type==null && other.getWork_view_type()==null) || 
             (this.work_view_type!=null &&
              this.work_view_type.equals(other.getWork_view_type())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getItem_id() != null) {
            _hashCode += getItem_id().hashCode();
        }
        if (getRow_id() != null) {
            _hashCode += getRow_id().hashCode();
        }
        if (getTicket_identifier() != null) {
            _hashCode += getTicket_identifier().hashCode();
        }
        if (getTicket_type() != null) {
            _hashCode += getTicket_type().hashCode();
        }
        if (getWork_actual_date() != null) {
            _hashCode += getWork_actual_date().hashCode();
        }
        if (getWork_created_by() != null) {
            _hashCode += getWork_created_by().hashCode();
        }
        if (getWork_created_by_contact_id() != null) {
            _hashCode += getWork_created_by_contact_id().hashCode();
        }
        if (getWork_created_date() != null) {
            _hashCode += getWork_created_date().hashCode();
        }
        if (getWork_description() != null) {
            _hashCode += getWork_description().hashCode();
        }
        if (getWork_modified_by() != null) {
            _hashCode += getWork_modified_by().hashCode();
        }
        if (getWork_modified_by_contact_id() != null) {
            _hashCode += getWork_modified_by_contact_id().hashCode();
        }
        if (getWork_modified_date() != null) {
            _hashCode += getWork_modified_date().hashCode();
        }
        if (getWork_time_spent() != null) {
            _hashCode += getWork_time_spent().hashCode();
        }
        if (getWork_type() != null) {
            _hashCode += getWork_type().hashCode();
        }
        if (getWork_type_code() != null) {
            _hashCode += getWork_type_code().hashCode();
        }
        if (getWork_view_type() != null) {
            _hashCode += getWork_view_type().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Worklog.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Worklog"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("item_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "item_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("row_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "row_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_actual_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_actual_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_created_by");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_created_by"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_created_by_contact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_created_by_contact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_created_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_created_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_modified_by");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_modified_by"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_modified_by_contact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_modified_by_contact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_modified_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_modified_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_time_spent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_time_spent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_type_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_type_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_view_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_view_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
