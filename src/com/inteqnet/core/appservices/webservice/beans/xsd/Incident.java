/**
 * Incident.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.beans.xsd;

public class Incident  extends com.inteqnet.components.webservice.beans.xsd.Bean  implements java.io.Serializable {
    private java.lang.Integer affected_ci_id;

    private java.lang.String affected_ci_identifier;

    private java.lang.String affected_ci_name;

    private java.lang.String approvalList;

    private java.lang.Integer assigned_contact_id;

    private java.lang.Integer assigned_group_id;

    private java.lang.String assigned_to_group_name;

    private java.lang.String assigned_to_individual_name;

    private java.lang.Integer case_id;

    private java.lang.String cause;

    private java.lang.String ccti_category;

    private java.lang.String ccti_class;

    private java.lang.Integer ccti_id;

    private java.lang.String ccti_item;

    private java.lang.String ccti_type;

    private java.lang.String created_by_name;

    private java.lang.String created_date;

    private com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute[] custom_attributes;

    private java.lang.String description_long;

    private java.lang.Integer global_ticket_item_id;

    private java.lang.String last_closed_by_name;

    private java.lang.String last_closed_date;

    private java.lang.String last_used_action_id;

    private java.lang.String modified_by_name;

    private java.lang.String modified_date;

    private java.lang.Integer parent_row_id;

    private java.lang.String parent_ticket_identifier;

    private java.lang.Integer person1_address_id;

    private java.lang.String person1_alt_email;

    private java.lang.String person1_alt_phone;

    private java.lang.Integer person1_contact_id;

    private java.lang.String person1_lvl1_org_name;

    private java.lang.String person1_lvl2_org_name;

    private java.lang.String person1_lvl3_org_name;

    private java.lang.Integer person1_org_id;

    private java.lang.Integer person2_address_id;

    private java.lang.String person2_alt_email;

    private java.lang.String person2_alt_phone;

    private java.lang.Integer person2_contact_id;

    private java.lang.String person2_lvl1_org_name;

    private java.lang.String person2_lvl2_org_name;

    private java.lang.String person2_lvl3_org_name;

    private java.lang.Integer person2_org_id;

    private java.lang.String related_to_global_issue_id;

    private java.lang.String requested_for_name;

    private java.lang.String requester_name;

    private java.lang.String resolution;

    private java.lang.Integer row_id;

    private java.lang.String severity;

    private java.lang.Integer sla_holiday_id;

    private java.lang.String sla_holiday_name;

    private java.lang.String sla_start_date;

    private java.lang.Integer sla_tz_id;

    private java.lang.String sla_tz_name;

    private java.lang.String solution_used_from_item_case;

    private java.lang.Integer solution_used_from_item_id;

    private java.lang.String support_email_address;

    private java.lang.String this_ticket_is;

    private java.lang.String ticket_description;

    private java.lang.String ticket_identifier;

    private java.lang.String ticket_impact;

    private java.lang.Integer ticket_impact_code;

    private java.lang.String ticket_phase;

    private java.lang.String ticket_priority;

    private java.lang.Integer ticket_priority_code;

    private java.lang.String ticket_reason_code;

    private java.lang.Integer ticket_solution_id;

    private java.lang.String ticket_source;

    private java.lang.Integer ticket_source_code;

    private java.lang.String ticket_status;

    private java.lang.String ticket_urgency;

    private java.lang.Integer ticket_urgency_code;

    private java.lang.String total_worklog_time_spent;

    private java.lang.String vip_flag_person1;

    private java.lang.String vip_flag_person2;

    private java.lang.String work_actual_date;

    private java.lang.String work_description;

    private java.lang.Integer work_time_spent;

    private java.lang.String work_type;

    private java.lang.Integer work_type_code;

    private java.lang.String work_view_type;

    public Incident() {
    }

    public Incident(
           java.lang.Integer affected_ci_id,
           java.lang.String affected_ci_identifier,
           java.lang.String affected_ci_name,
           java.lang.String approvalList,
           java.lang.Integer assigned_contact_id,
           java.lang.Integer assigned_group_id,
           java.lang.String assigned_to_group_name,
           java.lang.String assigned_to_individual_name,
           java.lang.Integer case_id,
           java.lang.String cause,
           java.lang.String ccti_category,
           java.lang.String ccti_class,
           java.lang.Integer ccti_id,
           java.lang.String ccti_item,
           java.lang.String ccti_type,
           java.lang.String created_by_name,
           java.lang.String created_date,
           com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute[] custom_attributes,
           java.lang.String description_long,
           java.lang.Integer global_ticket_item_id,
           java.lang.String last_closed_by_name,
           java.lang.String last_closed_date,
           java.lang.String last_used_action_id,
           java.lang.String modified_by_name,
           java.lang.String modified_date,
           java.lang.Integer parent_row_id,
           java.lang.String parent_ticket_identifier,
           java.lang.Integer person1_address_id,
           java.lang.String person1_alt_email,
           java.lang.String person1_alt_phone,
           java.lang.Integer person1_contact_id,
           java.lang.String person1_lvl1_org_name,
           java.lang.String person1_lvl2_org_name,
           java.lang.String person1_lvl3_org_name,
           java.lang.Integer person1_org_id,
           java.lang.Integer person2_address_id,
           java.lang.String person2_alt_email,
           java.lang.String person2_alt_phone,
           java.lang.Integer person2_contact_id,
           java.lang.String person2_lvl1_org_name,
           java.lang.String person2_lvl2_org_name,
           java.lang.String person2_lvl3_org_name,
           java.lang.Integer person2_org_id,
           java.lang.String related_to_global_issue_id,
           java.lang.String requested_for_name,
           java.lang.String requester_name,
           java.lang.String resolution,
           java.lang.Integer row_id,
           java.lang.String severity,
           java.lang.Integer sla_holiday_id,
           java.lang.String sla_holiday_name,
           java.lang.String sla_start_date,
           java.lang.Integer sla_tz_id,
           java.lang.String sla_tz_name,
           java.lang.String solution_used_from_item_case,
           java.lang.Integer solution_used_from_item_id,
           java.lang.String support_email_address,
           java.lang.String this_ticket_is,
           java.lang.String ticket_description,
           java.lang.String ticket_identifier,
           java.lang.String ticket_impact,
           java.lang.Integer ticket_impact_code,
           java.lang.String ticket_phase,
           java.lang.String ticket_priority,
           java.lang.Integer ticket_priority_code,
           java.lang.String ticket_reason_code,
           java.lang.Integer ticket_solution_id,
           java.lang.String ticket_source,
           java.lang.Integer ticket_source_code,
           java.lang.String ticket_status,
           java.lang.String ticket_urgency,
           java.lang.Integer ticket_urgency_code,
           java.lang.String total_worklog_time_spent,
           java.lang.String vip_flag_person1,
           java.lang.String vip_flag_person2,
           java.lang.String work_actual_date,
           java.lang.String work_description,
           java.lang.Integer work_time_spent,
           java.lang.String work_type,
           java.lang.Integer work_type_code,
           java.lang.String work_view_type) {
        this.affected_ci_id = affected_ci_id;
        this.affected_ci_identifier = affected_ci_identifier;
        this.affected_ci_name = affected_ci_name;
        this.approvalList = approvalList;
        this.assigned_contact_id = assigned_contact_id;
        this.assigned_group_id = assigned_group_id;
        this.assigned_to_group_name = assigned_to_group_name;
        this.assigned_to_individual_name = assigned_to_individual_name;
        this.case_id = case_id;
        this.cause = cause;
        this.ccti_category = ccti_category;
        this.ccti_class = ccti_class;
        this.ccti_id = ccti_id;
        this.ccti_item = ccti_item;
        this.ccti_type = ccti_type;
        this.created_by_name = created_by_name;
        this.created_date = created_date;
        this.custom_attributes = custom_attributes;
        this.description_long = description_long;
        this.global_ticket_item_id = global_ticket_item_id;
        this.last_closed_by_name = last_closed_by_name;
        this.last_closed_date = last_closed_date;
        this.last_used_action_id = last_used_action_id;
        this.modified_by_name = modified_by_name;
        this.modified_date = modified_date;
        this.parent_row_id = parent_row_id;
        this.parent_ticket_identifier = parent_ticket_identifier;
        this.person1_address_id = person1_address_id;
        this.person1_alt_email = person1_alt_email;
        this.person1_alt_phone = person1_alt_phone;
        this.person1_contact_id = person1_contact_id;
        this.person1_lvl1_org_name = person1_lvl1_org_name;
        this.person1_lvl2_org_name = person1_lvl2_org_name;
        this.person1_lvl3_org_name = person1_lvl3_org_name;
        this.person1_org_id = person1_org_id;
        this.person2_address_id = person2_address_id;
        this.person2_alt_email = person2_alt_email;
        this.person2_alt_phone = person2_alt_phone;
        this.person2_contact_id = person2_contact_id;
        this.person2_lvl1_org_name = person2_lvl1_org_name;
        this.person2_lvl2_org_name = person2_lvl2_org_name;
        this.person2_lvl3_org_name = person2_lvl3_org_name;
        this.person2_org_id = person2_org_id;
        this.related_to_global_issue_id = related_to_global_issue_id;
        this.requested_for_name = requested_for_name;
        this.requester_name = requester_name;
        this.resolution = resolution;
        this.row_id = row_id;
        this.severity = severity;
        this.sla_holiday_id = sla_holiday_id;
        this.sla_holiday_name = sla_holiday_name;
        this.sla_start_date = sla_start_date;
        this.sla_tz_id = sla_tz_id;
        this.sla_tz_name = sla_tz_name;
        this.solution_used_from_item_case = solution_used_from_item_case;
        this.solution_used_from_item_id = solution_used_from_item_id;
        this.support_email_address = support_email_address;
        this.this_ticket_is = this_ticket_is;
        this.ticket_description = ticket_description;
        this.ticket_identifier = ticket_identifier;
        this.ticket_impact = ticket_impact;
        this.ticket_impact_code = ticket_impact_code;
        this.ticket_phase = ticket_phase;
        this.ticket_priority = ticket_priority;
        this.ticket_priority_code = ticket_priority_code;
        this.ticket_reason_code = ticket_reason_code;
        this.ticket_solution_id = ticket_solution_id;
        this.ticket_source = ticket_source;
        this.ticket_source_code = ticket_source_code;
        this.ticket_status = ticket_status;
        this.ticket_urgency = ticket_urgency;
        this.ticket_urgency_code = ticket_urgency_code;
        this.total_worklog_time_spent = total_worklog_time_spent;
        this.vip_flag_person1 = vip_flag_person1;
        this.vip_flag_person2 = vip_flag_person2;
        this.work_actual_date = work_actual_date;
        this.work_description = work_description;
        this.work_time_spent = work_time_spent;
        this.work_type = work_type;
        this.work_type_code = work_type_code;
        this.work_view_type = work_view_type;
    }


    /**
     * Gets the affected_ci_id value for this Incident.
     * 
     * @return affected_ci_id
     */
    public java.lang.Integer getAffected_ci_id() {
        return affected_ci_id;
    }


    /**
     * Sets the affected_ci_id value for this Incident.
     * 
     * @param affected_ci_id
     */
    public void setAffected_ci_id(java.lang.Integer affected_ci_id) {
        this.affected_ci_id = affected_ci_id;
    }


    /**
     * Gets the affected_ci_identifier value for this Incident.
     * 
     * @return affected_ci_identifier
     */
    public java.lang.String getAffected_ci_identifier() {
        return affected_ci_identifier;
    }


    /**
     * Sets the affected_ci_identifier value for this Incident.
     * 
     * @param affected_ci_identifier
     */
    public void setAffected_ci_identifier(java.lang.String affected_ci_identifier) {
        this.affected_ci_identifier = affected_ci_identifier;
    }


    /**
     * Gets the affected_ci_name value for this Incident.
     * 
     * @return affected_ci_name
     */
    public java.lang.String getAffected_ci_name() {
        return affected_ci_name;
    }


    /**
     * Sets the affected_ci_name value for this Incident.
     * 
     * @param affected_ci_name
     */
    public void setAffected_ci_name(java.lang.String affected_ci_name) {
        this.affected_ci_name = affected_ci_name;
    }


    /**
     * Gets the approvalList value for this Incident.
     * 
     * @return approvalList
     */
    public java.lang.String getApprovalList() {
        return approvalList;
    }


    /**
     * Sets the approvalList value for this Incident.
     * 
     * @param approvalList
     */
    public void setApprovalList(java.lang.String approvalList) {
        this.approvalList = approvalList;
    }


    /**
     * Gets the assigned_contact_id value for this Incident.
     * 
     * @return assigned_contact_id
     */
    public java.lang.Integer getAssigned_contact_id() {
        return assigned_contact_id;
    }


    /**
     * Sets the assigned_contact_id value for this Incident.
     * 
     * @param assigned_contact_id
     */
    public void setAssigned_contact_id(java.lang.Integer assigned_contact_id) {
        this.assigned_contact_id = assigned_contact_id;
    }


    /**
     * Gets the assigned_group_id value for this Incident.
     * 
     * @return assigned_group_id
     */
    public java.lang.Integer getAssigned_group_id() {
        return assigned_group_id;
    }


    /**
     * Sets the assigned_group_id value for this Incident.
     * 
     * @param assigned_group_id
     */
    public void setAssigned_group_id(java.lang.Integer assigned_group_id) {
        this.assigned_group_id = assigned_group_id;
    }


    /**
     * Gets the assigned_to_group_name value for this Incident.
     * 
     * @return assigned_to_group_name
     */
    public java.lang.String getAssigned_to_group_name() {
        return assigned_to_group_name;
    }


    /**
     * Sets the assigned_to_group_name value for this Incident.
     * 
     * @param assigned_to_group_name
     */
    public void setAssigned_to_group_name(java.lang.String assigned_to_group_name) {
        this.assigned_to_group_name = assigned_to_group_name;
    }


    /**
     * Gets the assigned_to_individual_name value for this Incident.
     * 
     * @return assigned_to_individual_name
     */
    public java.lang.String getAssigned_to_individual_name() {
        return assigned_to_individual_name;
    }


    /**
     * Sets the assigned_to_individual_name value for this Incident.
     * 
     * @param assigned_to_individual_name
     */
    public void setAssigned_to_individual_name(java.lang.String assigned_to_individual_name) {
        this.assigned_to_individual_name = assigned_to_individual_name;
    }


    /**
     * Gets the case_id value for this Incident.
     * 
     * @return case_id
     */
    public java.lang.Integer getCase_id() {
        return case_id;
    }


    /**
     * Sets the case_id value for this Incident.
     * 
     * @param case_id
     */
    public void setCase_id(java.lang.Integer case_id) {
        this.case_id = case_id;
    }


    /**
     * Gets the cause value for this Incident.
     * 
     * @return cause
     */
    public java.lang.String getCause() {
        return cause;
    }


    /**
     * Sets the cause value for this Incident.
     * 
     * @param cause
     */
    public void setCause(java.lang.String cause) {
        this.cause = cause;
    }


    /**
     * Gets the ccti_category value for this Incident.
     * 
     * @return ccti_category
     */
    public java.lang.String getCcti_category() {
        return ccti_category;
    }


    /**
     * Sets the ccti_category value for this Incident.
     * 
     * @param ccti_category
     */
    public void setCcti_category(java.lang.String ccti_category) {
        this.ccti_category = ccti_category;
    }


    /**
     * Gets the ccti_class value for this Incident.
     * 
     * @return ccti_class
     */
    public java.lang.String getCcti_class() {
        return ccti_class;
    }


    /**
     * Sets the ccti_class value for this Incident.
     * 
     * @param ccti_class
     */
    public void setCcti_class(java.lang.String ccti_class) {
        this.ccti_class = ccti_class;
    }


    /**
     * Gets the ccti_id value for this Incident.
     * 
     * @return ccti_id
     */
    public java.lang.Integer getCcti_id() {
        return ccti_id;
    }


    /**
     * Sets the ccti_id value for this Incident.
     * 
     * @param ccti_id
     */
    public void setCcti_id(java.lang.Integer ccti_id) {
        this.ccti_id = ccti_id;
    }


    /**
     * Gets the ccti_item value for this Incident.
     * 
     * @return ccti_item
     */
    public java.lang.String getCcti_item() {
        return ccti_item;
    }


    /**
     * Sets the ccti_item value for this Incident.
     * 
     * @param ccti_item
     */
    public void setCcti_item(java.lang.String ccti_item) {
        this.ccti_item = ccti_item;
    }


    /**
     * Gets the ccti_type value for this Incident.
     * 
     * @return ccti_type
     */
    public java.lang.String getCcti_type() {
        return ccti_type;
    }


    /**
     * Sets the ccti_type value for this Incident.
     * 
     * @param ccti_type
     */
    public void setCcti_type(java.lang.String ccti_type) {
        this.ccti_type = ccti_type;
    }


    /**
     * Gets the created_by_name value for this Incident.
     * 
     * @return created_by_name
     */
    public java.lang.String getCreated_by_name() {
        return created_by_name;
    }


    /**
     * Sets the created_by_name value for this Incident.
     * 
     * @param created_by_name
     */
    public void setCreated_by_name(java.lang.String created_by_name) {
        this.created_by_name = created_by_name;
    }


    /**
     * Gets the created_date value for this Incident.
     * 
     * @return created_date
     */
    public java.lang.String getCreated_date() {
        return created_date;
    }


    /**
     * Sets the created_date value for this Incident.
     * 
     * @param created_date
     */
    public void setCreated_date(java.lang.String created_date) {
        this.created_date = created_date;
    }


    /**
     * Gets the custom_attributes value for this Incident.
     * 
     * @return custom_attributes
     */
    public com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute[] getCustom_attributes() {
        return custom_attributes;
    }


    /**
     * Sets the custom_attributes value for this Incident.
     * 
     * @param custom_attributes
     */
    public void setCustom_attributes(com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute[] custom_attributes) {
        this.custom_attributes = custom_attributes;
    }

    public com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute getCustom_attributes(int i) {
        return this.custom_attributes[i];
    }

    public void setCustom_attributes(int i, com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute _value) {
        this.custom_attributes[i] = _value;
    }


    /**
     * Gets the description_long value for this Incident.
     * 
     * @return description_long
     */
    public java.lang.String getDescription_long() {
        return description_long;
    }


    /**
     * Sets the description_long value for this Incident.
     * 
     * @param description_long
     */
    public void setDescription_long(java.lang.String description_long) {
        this.description_long = description_long;
    }


    /**
     * Gets the global_ticket_item_id value for this Incident.
     * 
     * @return global_ticket_item_id
     */
    public java.lang.Integer getGlobal_ticket_item_id() {
        return global_ticket_item_id;
    }


    /**
     * Sets the global_ticket_item_id value for this Incident.
     * 
     * @param global_ticket_item_id
     */
    public void setGlobal_ticket_item_id(java.lang.Integer global_ticket_item_id) {
        this.global_ticket_item_id = global_ticket_item_id;
    }


    /**
     * Gets the last_closed_by_name value for this Incident.
     * 
     * @return last_closed_by_name
     */
    public java.lang.String getLast_closed_by_name() {
        return last_closed_by_name;
    }


    /**
     * Sets the last_closed_by_name value for this Incident.
     * 
     * @param last_closed_by_name
     */
    public void setLast_closed_by_name(java.lang.String last_closed_by_name) {
        this.last_closed_by_name = last_closed_by_name;
    }


    /**
     * Gets the last_closed_date value for this Incident.
     * 
     * @return last_closed_date
     */
    public java.lang.String getLast_closed_date() {
        return last_closed_date;
    }


    /**
     * Sets the last_closed_date value for this Incident.
     * 
     * @param last_closed_date
     */
    public void setLast_closed_date(java.lang.String last_closed_date) {
        this.last_closed_date = last_closed_date;
    }


    /**
     * Gets the last_used_action_id value for this Incident.
     * 
     * @return last_used_action_id
     */
    public java.lang.String getLast_used_action_id() {
        return last_used_action_id;
    }


    /**
     * Sets the last_used_action_id value for this Incident.
     * 
     * @param last_used_action_id
     */
    public void setLast_used_action_id(java.lang.String last_used_action_id) {
        this.last_used_action_id = last_used_action_id;
    }


    /**
     * Gets the modified_by_name value for this Incident.
     * 
     * @return modified_by_name
     */
    public java.lang.String getModified_by_name() {
        return modified_by_name;
    }


    /**
     * Sets the modified_by_name value for this Incident.
     * 
     * @param modified_by_name
     */
    public void setModified_by_name(java.lang.String modified_by_name) {
        this.modified_by_name = modified_by_name;
    }


    /**
     * Gets the modified_date value for this Incident.
     * 
     * @return modified_date
     */
    public java.lang.String getModified_date() {
        return modified_date;
    }


    /**
     * Sets the modified_date value for this Incident.
     * 
     * @param modified_date
     */
    public void setModified_date(java.lang.String modified_date) {
        this.modified_date = modified_date;
    }


    /**
     * Gets the parent_row_id value for this Incident.
     * 
     * @return parent_row_id
     */
    public java.lang.Integer getParent_row_id() {
        return parent_row_id;
    }


    /**
     * Sets the parent_row_id value for this Incident.
     * 
     * @param parent_row_id
     */
    public void setParent_row_id(java.lang.Integer parent_row_id) {
        this.parent_row_id = parent_row_id;
    }


    /**
     * Gets the parent_ticket_identifier value for this Incident.
     * 
     * @return parent_ticket_identifier
     */
    public java.lang.String getParent_ticket_identifier() {
        return parent_ticket_identifier;
    }


    /**
     * Sets the parent_ticket_identifier value for this Incident.
     * 
     * @param parent_ticket_identifier
     */
    public void setParent_ticket_identifier(java.lang.String parent_ticket_identifier) {
        this.parent_ticket_identifier = parent_ticket_identifier;
    }


    /**
     * Gets the person1_address_id value for this Incident.
     * 
     * @return person1_address_id
     */
    public java.lang.Integer getPerson1_address_id() {
        return person1_address_id;
    }


    /**
     * Sets the person1_address_id value for this Incident.
     * 
     * @param person1_address_id
     */
    public void setPerson1_address_id(java.lang.Integer person1_address_id) {
        this.person1_address_id = person1_address_id;
    }


    /**
     * Gets the person1_alt_email value for this Incident.
     * 
     * @return person1_alt_email
     */
    public java.lang.String getPerson1_alt_email() {
        return person1_alt_email;
    }


    /**
     * Sets the person1_alt_email value for this Incident.
     * 
     * @param person1_alt_email
     */
    public void setPerson1_alt_email(java.lang.String person1_alt_email) {
        this.person1_alt_email = person1_alt_email;
    }


    /**
     * Gets the person1_alt_phone value for this Incident.
     * 
     * @return person1_alt_phone
     */
    public java.lang.String getPerson1_alt_phone() {
        return person1_alt_phone;
    }


    /**
     * Sets the person1_alt_phone value for this Incident.
     * 
     * @param person1_alt_phone
     */
    public void setPerson1_alt_phone(java.lang.String person1_alt_phone) {
        this.person1_alt_phone = person1_alt_phone;
    }


    /**
     * Gets the person1_contact_id value for this Incident.
     * 
     * @return person1_contact_id
     */
    public java.lang.Integer getPerson1_contact_id() {
        return person1_contact_id;
    }


    /**
     * Sets the person1_contact_id value for this Incident.
     * 
     * @param person1_contact_id
     */
    public void setPerson1_contact_id(java.lang.Integer person1_contact_id) {
        this.person1_contact_id = person1_contact_id;
    }


    /**
     * Gets the person1_lvl1_org_name value for this Incident.
     * 
     * @return person1_lvl1_org_name
     */
    public java.lang.String getPerson1_lvl1_org_name() {
        return person1_lvl1_org_name;
    }


    /**
     * Sets the person1_lvl1_org_name value for this Incident.
     * 
     * @param person1_lvl1_org_name
     */
    public void setPerson1_lvl1_org_name(java.lang.String person1_lvl1_org_name) {
        this.person1_lvl1_org_name = person1_lvl1_org_name;
    }


    /**
     * Gets the person1_lvl2_org_name value for this Incident.
     * 
     * @return person1_lvl2_org_name
     */
    public java.lang.String getPerson1_lvl2_org_name() {
        return person1_lvl2_org_name;
    }


    /**
     * Sets the person1_lvl2_org_name value for this Incident.
     * 
     * @param person1_lvl2_org_name
     */
    public void setPerson1_lvl2_org_name(java.lang.String person1_lvl2_org_name) {
        this.person1_lvl2_org_name = person1_lvl2_org_name;
    }


    /**
     * Gets the person1_lvl3_org_name value for this Incident.
     * 
     * @return person1_lvl3_org_name
     */
    public java.lang.String getPerson1_lvl3_org_name() {
        return person1_lvl3_org_name;
    }


    /**
     * Sets the person1_lvl3_org_name value for this Incident.
     * 
     * @param person1_lvl3_org_name
     */
    public void setPerson1_lvl3_org_name(java.lang.String person1_lvl3_org_name) {
        this.person1_lvl3_org_name = person1_lvl3_org_name;
    }


    /**
     * Gets the person1_org_id value for this Incident.
     * 
     * @return person1_org_id
     */
    public java.lang.Integer getPerson1_org_id() {
        return person1_org_id;
    }


    /**
     * Sets the person1_org_id value for this Incident.
     * 
     * @param person1_org_id
     */
    public void setPerson1_org_id(java.lang.Integer person1_org_id) {
        this.person1_org_id = person1_org_id;
    }


    /**
     * Gets the person2_address_id value for this Incident.
     * 
     * @return person2_address_id
     */
    public java.lang.Integer getPerson2_address_id() {
        return person2_address_id;
    }


    /**
     * Sets the person2_address_id value for this Incident.
     * 
     * @param person2_address_id
     */
    public void setPerson2_address_id(java.lang.Integer person2_address_id) {
        this.person2_address_id = person2_address_id;
    }


    /**
     * Gets the person2_alt_email value for this Incident.
     * 
     * @return person2_alt_email
     */
    public java.lang.String getPerson2_alt_email() {
        return person2_alt_email;
    }


    /**
     * Sets the person2_alt_email value for this Incident.
     * 
     * @param person2_alt_email
     */
    public void setPerson2_alt_email(java.lang.String person2_alt_email) {
        this.person2_alt_email = person2_alt_email;
    }


    /**
     * Gets the person2_alt_phone value for this Incident.
     * 
     * @return person2_alt_phone
     */
    public java.lang.String getPerson2_alt_phone() {
        return person2_alt_phone;
    }


    /**
     * Sets the person2_alt_phone value for this Incident.
     * 
     * @param person2_alt_phone
     */
    public void setPerson2_alt_phone(java.lang.String person2_alt_phone) {
        this.person2_alt_phone = person2_alt_phone;
    }


    /**
     * Gets the person2_contact_id value for this Incident.
     * 
     * @return person2_contact_id
     */
    public java.lang.Integer getPerson2_contact_id() {
        return person2_contact_id;
    }


    /**
     * Sets the person2_contact_id value for this Incident.
     * 
     * @param person2_contact_id
     */
    public void setPerson2_contact_id(java.lang.Integer person2_contact_id) {
        this.person2_contact_id = person2_contact_id;
    }


    /**
     * Gets the person2_lvl1_org_name value for this Incident.
     * 
     * @return person2_lvl1_org_name
     */
    public java.lang.String getPerson2_lvl1_org_name() {
        return person2_lvl1_org_name;
    }


    /**
     * Sets the person2_lvl1_org_name value for this Incident.
     * 
     * @param person2_lvl1_org_name
     */
    public void setPerson2_lvl1_org_name(java.lang.String person2_lvl1_org_name) {
        this.person2_lvl1_org_name = person2_lvl1_org_name;
    }


    /**
     * Gets the person2_lvl2_org_name value for this Incident.
     * 
     * @return person2_lvl2_org_name
     */
    public java.lang.String getPerson2_lvl2_org_name() {
        return person2_lvl2_org_name;
    }


    /**
     * Sets the person2_lvl2_org_name value for this Incident.
     * 
     * @param person2_lvl2_org_name
     */
    public void setPerson2_lvl2_org_name(java.lang.String person2_lvl2_org_name) {
        this.person2_lvl2_org_name = person2_lvl2_org_name;
    }


    /**
     * Gets the person2_lvl3_org_name value for this Incident.
     * 
     * @return person2_lvl3_org_name
     */
    public java.lang.String getPerson2_lvl3_org_name() {
        return person2_lvl3_org_name;
    }


    /**
     * Sets the person2_lvl3_org_name value for this Incident.
     * 
     * @param person2_lvl3_org_name
     */
    public void setPerson2_lvl3_org_name(java.lang.String person2_lvl3_org_name) {
        this.person2_lvl3_org_name = person2_lvl3_org_name;
    }


    /**
     * Gets the person2_org_id value for this Incident.
     * 
     * @return person2_org_id
     */
    public java.lang.Integer getPerson2_org_id() {
        return person2_org_id;
    }


    /**
     * Sets the person2_org_id value for this Incident.
     * 
     * @param person2_org_id
     */
    public void setPerson2_org_id(java.lang.Integer person2_org_id) {
        this.person2_org_id = person2_org_id;
    }


    /**
     * Gets the related_to_global_issue_id value for this Incident.
     * 
     * @return related_to_global_issue_id
     */
    public java.lang.String getRelated_to_global_issue_id() {
        return related_to_global_issue_id;
    }


    /**
     * Sets the related_to_global_issue_id value for this Incident.
     * 
     * @param related_to_global_issue_id
     */
    public void setRelated_to_global_issue_id(java.lang.String related_to_global_issue_id) {
        this.related_to_global_issue_id = related_to_global_issue_id;
    }


    /**
     * Gets the requested_for_name value for this Incident.
     * 
     * @return requested_for_name
     */
    public java.lang.String getRequested_for_name() {
        return requested_for_name;
    }


    /**
     * Sets the requested_for_name value for this Incident.
     * 
     * @param requested_for_name
     */
    public void setRequested_for_name(java.lang.String requested_for_name) {
        this.requested_for_name = requested_for_name;
    }


    /**
     * Gets the requester_name value for this Incident.
     * 
     * @return requester_name
     */
    public java.lang.String getRequester_name() {
        return requester_name;
    }


    /**
     * Sets the requester_name value for this Incident.
     * 
     * @param requester_name
     */
    public void setRequester_name(java.lang.String requester_name) {
        this.requester_name = requester_name;
    }


    /**
     * Gets the resolution value for this Incident.
     * 
     * @return resolution
     */
    public java.lang.String getResolution() {
        return resolution;
    }


    /**
     * Sets the resolution value for this Incident.
     * 
     * @param resolution
     */
    public void setResolution(java.lang.String resolution) {
        this.resolution = resolution;
    }


    /**
     * Gets the row_id value for this Incident.
     * 
     * @return row_id
     */
    public java.lang.Integer getRow_id() {
        return row_id;
    }


    /**
     * Sets the row_id value for this Incident.
     * 
     * @param row_id
     */
    public void setRow_id(java.lang.Integer row_id) {
        this.row_id = row_id;
    }


    /**
     * Gets the severity value for this Incident.
     * 
     * @return severity
     */
    public java.lang.String getSeverity() {
        return severity;
    }


    /**
     * Sets the severity value for this Incident.
     * 
     * @param severity
     */
    public void setSeverity(java.lang.String severity) {
        this.severity = severity;
    }


    /**
     * Gets the sla_holiday_id value for this Incident.
     * 
     * @return sla_holiday_id
     */
    public java.lang.Integer getSla_holiday_id() {
        return sla_holiday_id;
    }


    /**
     * Sets the sla_holiday_id value for this Incident.
     * 
     * @param sla_holiday_id
     */
    public void setSla_holiday_id(java.lang.Integer sla_holiday_id) {
        this.sla_holiday_id = sla_holiday_id;
    }


    /**
     * Gets the sla_holiday_name value for this Incident.
     * 
     * @return sla_holiday_name
     */
    public java.lang.String getSla_holiday_name() {
        return sla_holiday_name;
    }


    /**
     * Sets the sla_holiday_name value for this Incident.
     * 
     * @param sla_holiday_name
     */
    public void setSla_holiday_name(java.lang.String sla_holiday_name) {
        this.sla_holiday_name = sla_holiday_name;
    }


    /**
     * Gets the sla_start_date value for this Incident.
     * 
     * @return sla_start_date
     */
    public java.lang.String getSla_start_date() {
        return sla_start_date;
    }


    /**
     * Sets the sla_start_date value for this Incident.
     * 
     * @param sla_start_date
     */
    public void setSla_start_date(java.lang.String sla_start_date) {
        this.sla_start_date = sla_start_date;
    }


    /**
     * Gets the sla_tz_id value for this Incident.
     * 
     * @return sla_tz_id
     */
    public java.lang.Integer getSla_tz_id() {
        return sla_tz_id;
    }


    /**
     * Sets the sla_tz_id value for this Incident.
     * 
     * @param sla_tz_id
     */
    public void setSla_tz_id(java.lang.Integer sla_tz_id) {
        this.sla_tz_id = sla_tz_id;
    }


    /**
     * Gets the sla_tz_name value for this Incident.
     * 
     * @return sla_tz_name
     */
    public java.lang.String getSla_tz_name() {
        return sla_tz_name;
    }


    /**
     * Sets the sla_tz_name value for this Incident.
     * 
     * @param sla_tz_name
     */
    public void setSla_tz_name(java.lang.String sla_tz_name) {
        this.sla_tz_name = sla_tz_name;
    }


    /**
     * Gets the solution_used_from_item_case value for this Incident.
     * 
     * @return solution_used_from_item_case
     */
    public java.lang.String getSolution_used_from_item_case() {
        return solution_used_from_item_case;
    }


    /**
     * Sets the solution_used_from_item_case value for this Incident.
     * 
     * @param solution_used_from_item_case
     */
    public void setSolution_used_from_item_case(java.lang.String solution_used_from_item_case) {
        this.solution_used_from_item_case = solution_used_from_item_case;
    }


    /**
     * Gets the solution_used_from_item_id value for this Incident.
     * 
     * @return solution_used_from_item_id
     */
    public java.lang.Integer getSolution_used_from_item_id() {
        return solution_used_from_item_id;
    }


    /**
     * Sets the solution_used_from_item_id value for this Incident.
     * 
     * @param solution_used_from_item_id
     */
    public void setSolution_used_from_item_id(java.lang.Integer solution_used_from_item_id) {
        this.solution_used_from_item_id = solution_used_from_item_id;
    }


    /**
     * Gets the support_email_address value for this Incident.
     * 
     * @return support_email_address
     */
    public java.lang.String getSupport_email_address() {
        return support_email_address;
    }


    /**
     * Sets the support_email_address value for this Incident.
     * 
     * @param support_email_address
     */
    public void setSupport_email_address(java.lang.String support_email_address) {
        this.support_email_address = support_email_address;
    }


    /**
     * Gets the this_ticket_is value for this Incident.
     * 
     * @return this_ticket_is
     */
    public java.lang.String getThis_ticket_is() {
        return this_ticket_is;
    }


    /**
     * Sets the this_ticket_is value for this Incident.
     * 
     * @param this_ticket_is
     */
    public void setThis_ticket_is(java.lang.String this_ticket_is) {
        this.this_ticket_is = this_ticket_is;
    }


    /**
     * Gets the ticket_description value for this Incident.
     * 
     * @return ticket_description
     */
    public java.lang.String getTicket_description() {
        return ticket_description;
    }


    /**
     * Sets the ticket_description value for this Incident.
     * 
     * @param ticket_description
     */
    public void setTicket_description(java.lang.String ticket_description) {
        this.ticket_description = ticket_description;
    }


    /**
     * Gets the ticket_identifier value for this Incident.
     * 
     * @return ticket_identifier
     */
    public java.lang.String getTicket_identifier() {
        return ticket_identifier;
    }


    /**
     * Sets the ticket_identifier value for this Incident.
     * 
     * @param ticket_identifier
     */
    public void setTicket_identifier(java.lang.String ticket_identifier) {
        this.ticket_identifier = ticket_identifier;
    }


    /**
     * Gets the ticket_impact value for this Incident.
     * 
     * @return ticket_impact
     */
    public java.lang.String getTicket_impact() {
        return ticket_impact;
    }


    /**
     * Sets the ticket_impact value for this Incident.
     * 
     * @param ticket_impact
     */
    public void setTicket_impact(java.lang.String ticket_impact) {
        this.ticket_impact = ticket_impact;
    }


    /**
     * Gets the ticket_impact_code value for this Incident.
     * 
     * @return ticket_impact_code
     */
    public java.lang.Integer getTicket_impact_code() {
        return ticket_impact_code;
    }


    /**
     * Sets the ticket_impact_code value for this Incident.
     * 
     * @param ticket_impact_code
     */
    public void setTicket_impact_code(java.lang.Integer ticket_impact_code) {
        this.ticket_impact_code = ticket_impact_code;
    }


    /**
     * Gets the ticket_phase value for this Incident.
     * 
     * @return ticket_phase
     */
    public java.lang.String getTicket_phase() {
        return ticket_phase;
    }


    /**
     * Sets the ticket_phase value for this Incident.
     * 
     * @param ticket_phase
     */
    public void setTicket_phase(java.lang.String ticket_phase) {
        this.ticket_phase = ticket_phase;
    }


    /**
     * Gets the ticket_priority value for this Incident.
     * 
     * @return ticket_priority
     */
    public java.lang.String getTicket_priority() {
        return ticket_priority;
    }


    /**
     * Sets the ticket_priority value for this Incident.
     * 
     * @param ticket_priority
     */
    public void setTicket_priority(java.lang.String ticket_priority) {
        this.ticket_priority = ticket_priority;
    }


    /**
     * Gets the ticket_priority_code value for this Incident.
     * 
     * @return ticket_priority_code
     */
    public java.lang.Integer getTicket_priority_code() {
        return ticket_priority_code;
    }


    /**
     * Sets the ticket_priority_code value for this Incident.
     * 
     * @param ticket_priority_code
     */
    public void setTicket_priority_code(java.lang.Integer ticket_priority_code) {
        this.ticket_priority_code = ticket_priority_code;
    }


    /**
     * Gets the ticket_reason_code value for this Incident.
     * 
     * @return ticket_reason_code
     */
    public java.lang.String getTicket_reason_code() {
        return ticket_reason_code;
    }


    /**
     * Sets the ticket_reason_code value for this Incident.
     * 
     * @param ticket_reason_code
     */
    public void setTicket_reason_code(java.lang.String ticket_reason_code) {
        this.ticket_reason_code = ticket_reason_code;
    }


    /**
     * Gets the ticket_solution_id value for this Incident.
     * 
     * @return ticket_solution_id
     */
    public java.lang.Integer getTicket_solution_id() {
        return ticket_solution_id;
    }


    /**
     * Sets the ticket_solution_id value for this Incident.
     * 
     * @param ticket_solution_id
     */
    public void setTicket_solution_id(java.lang.Integer ticket_solution_id) {
        this.ticket_solution_id = ticket_solution_id;
    }


    /**
     * Gets the ticket_source value for this Incident.
     * 
     * @return ticket_source
     */
    public java.lang.String getTicket_source() {
        return ticket_source;
    }


    /**
     * Sets the ticket_source value for this Incident.
     * 
     * @param ticket_source
     */
    public void setTicket_source(java.lang.String ticket_source) {
        this.ticket_source = ticket_source;
    }


    /**
     * Gets the ticket_source_code value for this Incident.
     * 
     * @return ticket_source_code
     */
    public java.lang.Integer getTicket_source_code() {
        return ticket_source_code;
    }


    /**
     * Sets the ticket_source_code value for this Incident.
     * 
     * @param ticket_source_code
     */
    public void setTicket_source_code(java.lang.Integer ticket_source_code) {
        this.ticket_source_code = ticket_source_code;
    }


    /**
     * Gets the ticket_status value for this Incident.
     * 
     * @return ticket_status
     */
    public java.lang.String getTicket_status() {
        return ticket_status;
    }


    /**
     * Sets the ticket_status value for this Incident.
     * 
     * @param ticket_status
     */
    public void setTicket_status(java.lang.String ticket_status) {
        this.ticket_status = ticket_status;
    }


    /**
     * Gets the ticket_urgency value for this Incident.
     * 
     * @return ticket_urgency
     */
    public java.lang.String getTicket_urgency() {
        return ticket_urgency;
    }


    /**
     * Sets the ticket_urgency value for this Incident.
     * 
     * @param ticket_urgency
     */
    public void setTicket_urgency(java.lang.String ticket_urgency) {
        this.ticket_urgency = ticket_urgency;
    }


    /**
     * Gets the ticket_urgency_code value for this Incident.
     * 
     * @return ticket_urgency_code
     */
    public java.lang.Integer getTicket_urgency_code() {
        return ticket_urgency_code;
    }


    /**
     * Sets the ticket_urgency_code value for this Incident.
     * 
     * @param ticket_urgency_code
     */
    public void setTicket_urgency_code(java.lang.Integer ticket_urgency_code) {
        this.ticket_urgency_code = ticket_urgency_code;
    }


    /**
     * Gets the total_worklog_time_spent value for this Incident.
     * 
     * @return total_worklog_time_spent
     */
    public java.lang.String getTotal_worklog_time_spent() {
        return total_worklog_time_spent;
    }


    /**
     * Sets the total_worklog_time_spent value for this Incident.
     * 
     * @param total_worklog_time_spent
     */
    public void setTotal_worklog_time_spent(java.lang.String total_worklog_time_spent) {
        this.total_worklog_time_spent = total_worklog_time_spent;
    }


    /**
     * Gets the vip_flag_person1 value for this Incident.
     * 
     * @return vip_flag_person1
     */
    public java.lang.String getVip_flag_person1() {
        return vip_flag_person1;
    }


    /**
     * Sets the vip_flag_person1 value for this Incident.
     * 
     * @param vip_flag_person1
     */
    public void setVip_flag_person1(java.lang.String vip_flag_person1) {
        this.vip_flag_person1 = vip_flag_person1;
    }


    /**
     * Gets the vip_flag_person2 value for this Incident.
     * 
     * @return vip_flag_person2
     */
    public java.lang.String getVip_flag_person2() {
        return vip_flag_person2;
    }


    /**
     * Sets the vip_flag_person2 value for this Incident.
     * 
     * @param vip_flag_person2
     */
    public void setVip_flag_person2(java.lang.String vip_flag_person2) {
        this.vip_flag_person2 = vip_flag_person2;
    }


    /**
     * Gets the work_actual_date value for this Incident.
     * 
     * @return work_actual_date
     */
    public java.lang.String getWork_actual_date() {
        return work_actual_date;
    }


    /**
     * Sets the work_actual_date value for this Incident.
     * 
     * @param work_actual_date
     */
    public void setWork_actual_date(java.lang.String work_actual_date) {
        this.work_actual_date = work_actual_date;
    }


    /**
     * Gets the work_description value for this Incident.
     * 
     * @return work_description
     */
    public java.lang.String getWork_description() {
        return work_description;
    }


    /**
     * Sets the work_description value for this Incident.
     * 
     * @param work_description
     */
    public void setWork_description(java.lang.String work_description) {
        this.work_description = work_description;
    }


    /**
     * Gets the work_time_spent value for this Incident.
     * 
     * @return work_time_spent
     */
    public java.lang.Integer getWork_time_spent() {
        return work_time_spent;
    }


    /**
     * Sets the work_time_spent value for this Incident.
     * 
     * @param work_time_spent
     */
    public void setWork_time_spent(java.lang.Integer work_time_spent) {
        this.work_time_spent = work_time_spent;
    }


    /**
     * Gets the work_type value for this Incident.
     * 
     * @return work_type
     */
    public java.lang.String getWork_type() {
        return work_type;
    }


    /**
     * Sets the work_type value for this Incident.
     * 
     * @param work_type
     */
    public void setWork_type(java.lang.String work_type) {
        this.work_type = work_type;
    }


    /**
     * Gets the work_type_code value for this Incident.
     * 
     * @return work_type_code
     */
    public java.lang.Integer getWork_type_code() {
        return work_type_code;
    }


    /**
     * Sets the work_type_code value for this Incident.
     * 
     * @param work_type_code
     */
    public void setWork_type_code(java.lang.Integer work_type_code) {
        this.work_type_code = work_type_code;
    }


    /**
     * Gets the work_view_type value for this Incident.
     * 
     * @return work_view_type
     */
    public java.lang.String getWork_view_type() {
        return work_view_type;
    }


    /**
     * Sets the work_view_type value for this Incident.
     * 
     * @param work_view_type
     */
    public void setWork_view_type(java.lang.String work_view_type) {
        this.work_view_type = work_view_type;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Incident)) return false;
        Incident other = (Incident) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.affected_ci_id==null && other.getAffected_ci_id()==null) || 
             (this.affected_ci_id!=null &&
              this.affected_ci_id.equals(other.getAffected_ci_id()))) &&
            ((this.affected_ci_identifier==null && other.getAffected_ci_identifier()==null) || 
             (this.affected_ci_identifier!=null &&
              this.affected_ci_identifier.equals(other.getAffected_ci_identifier()))) &&
            ((this.affected_ci_name==null && other.getAffected_ci_name()==null) || 
             (this.affected_ci_name!=null &&
              this.affected_ci_name.equals(other.getAffected_ci_name()))) &&
            ((this.approvalList==null && other.getApprovalList()==null) || 
             (this.approvalList!=null &&
              this.approvalList.equals(other.getApprovalList()))) &&
            ((this.assigned_contact_id==null && other.getAssigned_contact_id()==null) || 
             (this.assigned_contact_id!=null &&
              this.assigned_contact_id.equals(other.getAssigned_contact_id()))) &&
            ((this.assigned_group_id==null && other.getAssigned_group_id()==null) || 
             (this.assigned_group_id!=null &&
              this.assigned_group_id.equals(other.getAssigned_group_id()))) &&
            ((this.assigned_to_group_name==null && other.getAssigned_to_group_name()==null) || 
             (this.assigned_to_group_name!=null &&
              this.assigned_to_group_name.equals(other.getAssigned_to_group_name()))) &&
            ((this.assigned_to_individual_name==null && other.getAssigned_to_individual_name()==null) || 
             (this.assigned_to_individual_name!=null &&
              this.assigned_to_individual_name.equals(other.getAssigned_to_individual_name()))) &&
            ((this.case_id==null && other.getCase_id()==null) || 
             (this.case_id!=null &&
              this.case_id.equals(other.getCase_id()))) &&
            ((this.cause==null && other.getCause()==null) || 
             (this.cause!=null &&
              this.cause.equals(other.getCause()))) &&
            ((this.ccti_category==null && other.getCcti_category()==null) || 
             (this.ccti_category!=null &&
              this.ccti_category.equals(other.getCcti_category()))) &&
            ((this.ccti_class==null && other.getCcti_class()==null) || 
             (this.ccti_class!=null &&
              this.ccti_class.equals(other.getCcti_class()))) &&
            ((this.ccti_id==null && other.getCcti_id()==null) || 
             (this.ccti_id!=null &&
              this.ccti_id.equals(other.getCcti_id()))) &&
            ((this.ccti_item==null && other.getCcti_item()==null) || 
             (this.ccti_item!=null &&
              this.ccti_item.equals(other.getCcti_item()))) &&
            ((this.ccti_type==null && other.getCcti_type()==null) || 
             (this.ccti_type!=null &&
              this.ccti_type.equals(other.getCcti_type()))) &&
            ((this.created_by_name==null && other.getCreated_by_name()==null) || 
             (this.created_by_name!=null &&
              this.created_by_name.equals(other.getCreated_by_name()))) &&
            ((this.created_date==null && other.getCreated_date()==null) || 
             (this.created_date!=null &&
              this.created_date.equals(other.getCreated_date()))) &&
            ((this.custom_attributes==null && other.getCustom_attributes()==null) || 
             (this.custom_attributes!=null &&
              java.util.Arrays.equals(this.custom_attributes, other.getCustom_attributes()))) &&
            ((this.description_long==null && other.getDescription_long()==null) || 
             (this.description_long!=null &&
              this.description_long.equals(other.getDescription_long()))) &&
            ((this.global_ticket_item_id==null && other.getGlobal_ticket_item_id()==null) || 
             (this.global_ticket_item_id!=null &&
              this.global_ticket_item_id.equals(other.getGlobal_ticket_item_id()))) &&
            ((this.last_closed_by_name==null && other.getLast_closed_by_name()==null) || 
             (this.last_closed_by_name!=null &&
              this.last_closed_by_name.equals(other.getLast_closed_by_name()))) &&
            ((this.last_closed_date==null && other.getLast_closed_date()==null) || 
             (this.last_closed_date!=null &&
              this.last_closed_date.equals(other.getLast_closed_date()))) &&
            ((this.last_used_action_id==null && other.getLast_used_action_id()==null) || 
             (this.last_used_action_id!=null &&
              this.last_used_action_id.equals(other.getLast_used_action_id()))) &&
            ((this.modified_by_name==null && other.getModified_by_name()==null) || 
             (this.modified_by_name!=null &&
              this.modified_by_name.equals(other.getModified_by_name()))) &&
            ((this.modified_date==null && other.getModified_date()==null) || 
             (this.modified_date!=null &&
              this.modified_date.equals(other.getModified_date()))) &&
            ((this.parent_row_id==null && other.getParent_row_id()==null) || 
             (this.parent_row_id!=null &&
              this.parent_row_id.equals(other.getParent_row_id()))) &&
            ((this.parent_ticket_identifier==null && other.getParent_ticket_identifier()==null) || 
             (this.parent_ticket_identifier!=null &&
              this.parent_ticket_identifier.equals(other.getParent_ticket_identifier()))) &&
            ((this.person1_address_id==null && other.getPerson1_address_id()==null) || 
             (this.person1_address_id!=null &&
              this.person1_address_id.equals(other.getPerson1_address_id()))) &&
            ((this.person1_alt_email==null && other.getPerson1_alt_email()==null) || 
             (this.person1_alt_email!=null &&
              this.person1_alt_email.equals(other.getPerson1_alt_email()))) &&
            ((this.person1_alt_phone==null && other.getPerson1_alt_phone()==null) || 
             (this.person1_alt_phone!=null &&
              this.person1_alt_phone.equals(other.getPerson1_alt_phone()))) &&
            ((this.person1_contact_id==null && other.getPerson1_contact_id()==null) || 
             (this.person1_contact_id!=null &&
              this.person1_contact_id.equals(other.getPerson1_contact_id()))) &&
            ((this.person1_lvl1_org_name==null && other.getPerson1_lvl1_org_name()==null) || 
             (this.person1_lvl1_org_name!=null &&
              this.person1_lvl1_org_name.equals(other.getPerson1_lvl1_org_name()))) &&
            ((this.person1_lvl2_org_name==null && other.getPerson1_lvl2_org_name()==null) || 
             (this.person1_lvl2_org_name!=null &&
              this.person1_lvl2_org_name.equals(other.getPerson1_lvl2_org_name()))) &&
            ((this.person1_lvl3_org_name==null && other.getPerson1_lvl3_org_name()==null) || 
             (this.person1_lvl3_org_name!=null &&
              this.person1_lvl3_org_name.equals(other.getPerson1_lvl3_org_name()))) &&
            ((this.person1_org_id==null && other.getPerson1_org_id()==null) || 
             (this.person1_org_id!=null &&
              this.person1_org_id.equals(other.getPerson1_org_id()))) &&
            ((this.person2_address_id==null && other.getPerson2_address_id()==null) || 
             (this.person2_address_id!=null &&
              this.person2_address_id.equals(other.getPerson2_address_id()))) &&
            ((this.person2_alt_email==null && other.getPerson2_alt_email()==null) || 
             (this.person2_alt_email!=null &&
              this.person2_alt_email.equals(other.getPerson2_alt_email()))) &&
            ((this.person2_alt_phone==null && other.getPerson2_alt_phone()==null) || 
             (this.person2_alt_phone!=null &&
              this.person2_alt_phone.equals(other.getPerson2_alt_phone()))) &&
            ((this.person2_contact_id==null && other.getPerson2_contact_id()==null) || 
             (this.person2_contact_id!=null &&
              this.person2_contact_id.equals(other.getPerson2_contact_id()))) &&
            ((this.person2_lvl1_org_name==null && other.getPerson2_lvl1_org_name()==null) || 
             (this.person2_lvl1_org_name!=null &&
              this.person2_lvl1_org_name.equals(other.getPerson2_lvl1_org_name()))) &&
            ((this.person2_lvl2_org_name==null && other.getPerson2_lvl2_org_name()==null) || 
             (this.person2_lvl2_org_name!=null &&
              this.person2_lvl2_org_name.equals(other.getPerson2_lvl2_org_name()))) &&
            ((this.person2_lvl3_org_name==null && other.getPerson2_lvl3_org_name()==null) || 
             (this.person2_lvl3_org_name!=null &&
              this.person2_lvl3_org_name.equals(other.getPerson2_lvl3_org_name()))) &&
            ((this.person2_org_id==null && other.getPerson2_org_id()==null) || 
             (this.person2_org_id!=null &&
              this.person2_org_id.equals(other.getPerson2_org_id()))) &&
            ((this.related_to_global_issue_id==null && other.getRelated_to_global_issue_id()==null) || 
             (this.related_to_global_issue_id!=null &&
              this.related_to_global_issue_id.equals(other.getRelated_to_global_issue_id()))) &&
            ((this.requested_for_name==null && other.getRequested_for_name()==null) || 
             (this.requested_for_name!=null &&
              this.requested_for_name.equals(other.getRequested_for_name()))) &&
            ((this.requester_name==null && other.getRequester_name()==null) || 
             (this.requester_name!=null &&
              this.requester_name.equals(other.getRequester_name()))) &&
            ((this.resolution==null && other.getResolution()==null) || 
             (this.resolution!=null &&
              this.resolution.equals(other.getResolution()))) &&
            ((this.row_id==null && other.getRow_id()==null) || 
             (this.row_id!=null &&
              this.row_id.equals(other.getRow_id()))) &&
            ((this.severity==null && other.getSeverity()==null) || 
             (this.severity!=null &&
              this.severity.equals(other.getSeverity()))) &&
            ((this.sla_holiday_id==null && other.getSla_holiday_id()==null) || 
             (this.sla_holiday_id!=null &&
              this.sla_holiday_id.equals(other.getSla_holiday_id()))) &&
            ((this.sla_holiday_name==null && other.getSla_holiday_name()==null) || 
             (this.sla_holiday_name!=null &&
              this.sla_holiday_name.equals(other.getSla_holiday_name()))) &&
            ((this.sla_start_date==null && other.getSla_start_date()==null) || 
             (this.sla_start_date!=null &&
              this.sla_start_date.equals(other.getSla_start_date()))) &&
            ((this.sla_tz_id==null && other.getSla_tz_id()==null) || 
             (this.sla_tz_id!=null &&
              this.sla_tz_id.equals(other.getSla_tz_id()))) &&
            ((this.sla_tz_name==null && other.getSla_tz_name()==null) || 
             (this.sla_tz_name!=null &&
              this.sla_tz_name.equals(other.getSla_tz_name()))) &&
            ((this.solution_used_from_item_case==null && other.getSolution_used_from_item_case()==null) || 
             (this.solution_used_from_item_case!=null &&
              this.solution_used_from_item_case.equals(other.getSolution_used_from_item_case()))) &&
            ((this.solution_used_from_item_id==null && other.getSolution_used_from_item_id()==null) || 
             (this.solution_used_from_item_id!=null &&
              this.solution_used_from_item_id.equals(other.getSolution_used_from_item_id()))) &&
            ((this.support_email_address==null && other.getSupport_email_address()==null) || 
             (this.support_email_address!=null &&
              this.support_email_address.equals(other.getSupport_email_address()))) &&
            ((this.this_ticket_is==null && other.getThis_ticket_is()==null) || 
             (this.this_ticket_is!=null &&
              this.this_ticket_is.equals(other.getThis_ticket_is()))) &&
            ((this.ticket_description==null && other.getTicket_description()==null) || 
             (this.ticket_description!=null &&
              this.ticket_description.equals(other.getTicket_description()))) &&
            ((this.ticket_identifier==null && other.getTicket_identifier()==null) || 
             (this.ticket_identifier!=null &&
              this.ticket_identifier.equals(other.getTicket_identifier()))) &&
            ((this.ticket_impact==null && other.getTicket_impact()==null) || 
             (this.ticket_impact!=null &&
              this.ticket_impact.equals(other.getTicket_impact()))) &&
            ((this.ticket_impact_code==null && other.getTicket_impact_code()==null) || 
             (this.ticket_impact_code!=null &&
              this.ticket_impact_code.equals(other.getTicket_impact_code()))) &&
            ((this.ticket_phase==null && other.getTicket_phase()==null) || 
             (this.ticket_phase!=null &&
              this.ticket_phase.equals(other.getTicket_phase()))) &&
            ((this.ticket_priority==null && other.getTicket_priority()==null) || 
             (this.ticket_priority!=null &&
              this.ticket_priority.equals(other.getTicket_priority()))) &&
            ((this.ticket_priority_code==null && other.getTicket_priority_code()==null) || 
             (this.ticket_priority_code!=null &&
              this.ticket_priority_code.equals(other.getTicket_priority_code()))) &&
            ((this.ticket_reason_code==null && other.getTicket_reason_code()==null) || 
             (this.ticket_reason_code!=null &&
              this.ticket_reason_code.equals(other.getTicket_reason_code()))) &&
            ((this.ticket_solution_id==null && other.getTicket_solution_id()==null) || 
             (this.ticket_solution_id!=null &&
              this.ticket_solution_id.equals(other.getTicket_solution_id()))) &&
            ((this.ticket_source==null && other.getTicket_source()==null) || 
             (this.ticket_source!=null &&
              this.ticket_source.equals(other.getTicket_source()))) &&
            ((this.ticket_source_code==null && other.getTicket_source_code()==null) || 
             (this.ticket_source_code!=null &&
              this.ticket_source_code.equals(other.getTicket_source_code()))) &&
            ((this.ticket_status==null && other.getTicket_status()==null) || 
             (this.ticket_status!=null &&
              this.ticket_status.equals(other.getTicket_status()))) &&
            ((this.ticket_urgency==null && other.getTicket_urgency()==null) || 
             (this.ticket_urgency!=null &&
              this.ticket_urgency.equals(other.getTicket_urgency()))) &&
            ((this.ticket_urgency_code==null && other.getTicket_urgency_code()==null) || 
             (this.ticket_urgency_code!=null &&
              this.ticket_urgency_code.equals(other.getTicket_urgency_code()))) &&
            ((this.total_worklog_time_spent==null && other.getTotal_worklog_time_spent()==null) || 
             (this.total_worklog_time_spent!=null &&
              this.total_worklog_time_spent.equals(other.getTotal_worklog_time_spent()))) &&
            ((this.vip_flag_person1==null && other.getVip_flag_person1()==null) || 
             (this.vip_flag_person1!=null &&
              this.vip_flag_person1.equals(other.getVip_flag_person1()))) &&
            ((this.vip_flag_person2==null && other.getVip_flag_person2()==null) || 
             (this.vip_flag_person2!=null &&
              this.vip_flag_person2.equals(other.getVip_flag_person2()))) &&
            ((this.work_actual_date==null && other.getWork_actual_date()==null) || 
             (this.work_actual_date!=null &&
              this.work_actual_date.equals(other.getWork_actual_date()))) &&
            ((this.work_description==null && other.getWork_description()==null) || 
             (this.work_description!=null &&
              this.work_description.equals(other.getWork_description()))) &&
            ((this.work_time_spent==null && other.getWork_time_spent()==null) || 
             (this.work_time_spent!=null &&
              this.work_time_spent.equals(other.getWork_time_spent()))) &&
            ((this.work_type==null && other.getWork_type()==null) || 
             (this.work_type!=null &&
              this.work_type.equals(other.getWork_type()))) &&
            ((this.work_type_code==null && other.getWork_type_code()==null) || 
             (this.work_type_code!=null &&
              this.work_type_code.equals(other.getWork_type_code()))) &&
            ((this.work_view_type==null && other.getWork_view_type()==null) || 
             (this.work_view_type!=null &&
              this.work_view_type.equals(other.getWork_view_type())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAffected_ci_id() != null) {
            _hashCode += getAffected_ci_id().hashCode();
        }
        if (getAffected_ci_identifier() != null) {
            _hashCode += getAffected_ci_identifier().hashCode();
        }
        if (getAffected_ci_name() != null) {
            _hashCode += getAffected_ci_name().hashCode();
        }
        if (getApprovalList() != null) {
            _hashCode += getApprovalList().hashCode();
        }
        if (getAssigned_contact_id() != null) {
            _hashCode += getAssigned_contact_id().hashCode();
        }
        if (getAssigned_group_id() != null) {
            _hashCode += getAssigned_group_id().hashCode();
        }
        if (getAssigned_to_group_name() != null) {
            _hashCode += getAssigned_to_group_name().hashCode();
        }
        if (getAssigned_to_individual_name() != null) {
            _hashCode += getAssigned_to_individual_name().hashCode();
        }
        if (getCase_id() != null) {
            _hashCode += getCase_id().hashCode();
        }
        if (getCause() != null) {
            _hashCode += getCause().hashCode();
        }
        if (getCcti_category() != null) {
            _hashCode += getCcti_category().hashCode();
        }
        if (getCcti_class() != null) {
            _hashCode += getCcti_class().hashCode();
        }
        if (getCcti_id() != null) {
            _hashCode += getCcti_id().hashCode();
        }
        if (getCcti_item() != null) {
            _hashCode += getCcti_item().hashCode();
        }
        if (getCcti_type() != null) {
            _hashCode += getCcti_type().hashCode();
        }
        if (getCreated_by_name() != null) {
            _hashCode += getCreated_by_name().hashCode();
        }
        if (getCreated_date() != null) {
            _hashCode += getCreated_date().hashCode();
        }
        if (getCustom_attributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustom_attributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustom_attributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDescription_long() != null) {
            _hashCode += getDescription_long().hashCode();
        }
        if (getGlobal_ticket_item_id() != null) {
            _hashCode += getGlobal_ticket_item_id().hashCode();
        }
        if (getLast_closed_by_name() != null) {
            _hashCode += getLast_closed_by_name().hashCode();
        }
        if (getLast_closed_date() != null) {
            _hashCode += getLast_closed_date().hashCode();
        }
        if (getLast_used_action_id() != null) {
            _hashCode += getLast_used_action_id().hashCode();
        }
        if (getModified_by_name() != null) {
            _hashCode += getModified_by_name().hashCode();
        }
        if (getModified_date() != null) {
            _hashCode += getModified_date().hashCode();
        }
        if (getParent_row_id() != null) {
            _hashCode += getParent_row_id().hashCode();
        }
        if (getParent_ticket_identifier() != null) {
            _hashCode += getParent_ticket_identifier().hashCode();
        }
        if (getPerson1_address_id() != null) {
            _hashCode += getPerson1_address_id().hashCode();
        }
        if (getPerson1_alt_email() != null) {
            _hashCode += getPerson1_alt_email().hashCode();
        }
        if (getPerson1_alt_phone() != null) {
            _hashCode += getPerson1_alt_phone().hashCode();
        }
        if (getPerson1_contact_id() != null) {
            _hashCode += getPerson1_contact_id().hashCode();
        }
        if (getPerson1_lvl1_org_name() != null) {
            _hashCode += getPerson1_lvl1_org_name().hashCode();
        }
        if (getPerson1_lvl2_org_name() != null) {
            _hashCode += getPerson1_lvl2_org_name().hashCode();
        }
        if (getPerson1_lvl3_org_name() != null) {
            _hashCode += getPerson1_lvl3_org_name().hashCode();
        }
        if (getPerson1_org_id() != null) {
            _hashCode += getPerson1_org_id().hashCode();
        }
        if (getPerson2_address_id() != null) {
            _hashCode += getPerson2_address_id().hashCode();
        }
        if (getPerson2_alt_email() != null) {
            _hashCode += getPerson2_alt_email().hashCode();
        }
        if (getPerson2_alt_phone() != null) {
            _hashCode += getPerson2_alt_phone().hashCode();
        }
        if (getPerson2_contact_id() != null) {
            _hashCode += getPerson2_contact_id().hashCode();
        }
        if (getPerson2_lvl1_org_name() != null) {
            _hashCode += getPerson2_lvl1_org_name().hashCode();
        }
        if (getPerson2_lvl2_org_name() != null) {
            _hashCode += getPerson2_lvl2_org_name().hashCode();
        }
        if (getPerson2_lvl3_org_name() != null) {
            _hashCode += getPerson2_lvl3_org_name().hashCode();
        }
        if (getPerson2_org_id() != null) {
            _hashCode += getPerson2_org_id().hashCode();
        }
        if (getRelated_to_global_issue_id() != null) {
            _hashCode += getRelated_to_global_issue_id().hashCode();
        }
        if (getRequested_for_name() != null) {
            _hashCode += getRequested_for_name().hashCode();
        }
        if (getRequester_name() != null) {
            _hashCode += getRequester_name().hashCode();
        }
        if (getResolution() != null) {
            _hashCode += getResolution().hashCode();
        }
        if (getRow_id() != null) {
            _hashCode += getRow_id().hashCode();
        }
        if (getSeverity() != null) {
            _hashCode += getSeverity().hashCode();
        }
        if (getSla_holiday_id() != null) {
            _hashCode += getSla_holiday_id().hashCode();
        }
        if (getSla_holiday_name() != null) {
            _hashCode += getSla_holiday_name().hashCode();
        }
        if (getSla_start_date() != null) {
            _hashCode += getSla_start_date().hashCode();
        }
        if (getSla_tz_id() != null) {
            _hashCode += getSla_tz_id().hashCode();
        }
        if (getSla_tz_name() != null) {
            _hashCode += getSla_tz_name().hashCode();
        }
        if (getSolution_used_from_item_case() != null) {
            _hashCode += getSolution_used_from_item_case().hashCode();
        }
        if (getSolution_used_from_item_id() != null) {
            _hashCode += getSolution_used_from_item_id().hashCode();
        }
        if (getSupport_email_address() != null) {
            _hashCode += getSupport_email_address().hashCode();
        }
        if (getThis_ticket_is() != null) {
            _hashCode += getThis_ticket_is().hashCode();
        }
        if (getTicket_description() != null) {
            _hashCode += getTicket_description().hashCode();
        }
        if (getTicket_identifier() != null) {
            _hashCode += getTicket_identifier().hashCode();
        }
        if (getTicket_impact() != null) {
            _hashCode += getTicket_impact().hashCode();
        }
        if (getTicket_impact_code() != null) {
            _hashCode += getTicket_impact_code().hashCode();
        }
        if (getTicket_phase() != null) {
            _hashCode += getTicket_phase().hashCode();
        }
        if (getTicket_priority() != null) {
            _hashCode += getTicket_priority().hashCode();
        }
        if (getTicket_priority_code() != null) {
            _hashCode += getTicket_priority_code().hashCode();
        }
        if (getTicket_reason_code() != null) {
            _hashCode += getTicket_reason_code().hashCode();
        }
        if (getTicket_solution_id() != null) {
            _hashCode += getTicket_solution_id().hashCode();
        }
        if (getTicket_source() != null) {
            _hashCode += getTicket_source().hashCode();
        }
        if (getTicket_source_code() != null) {
            _hashCode += getTicket_source_code().hashCode();
        }
        if (getTicket_status() != null) {
            _hashCode += getTicket_status().hashCode();
        }
        if (getTicket_urgency() != null) {
            _hashCode += getTicket_urgency().hashCode();
        }
        if (getTicket_urgency_code() != null) {
            _hashCode += getTicket_urgency_code().hashCode();
        }
        if (getTotal_worklog_time_spent() != null) {
            _hashCode += getTotal_worklog_time_spent().hashCode();
        }
        if (getVip_flag_person1() != null) {
            _hashCode += getVip_flag_person1().hashCode();
        }
        if (getVip_flag_person2() != null) {
            _hashCode += getVip_flag_person2().hashCode();
        }
        if (getWork_actual_date() != null) {
            _hashCode += getWork_actual_date().hashCode();
        }
        if (getWork_description() != null) {
            _hashCode += getWork_description().hashCode();
        }
        if (getWork_time_spent() != null) {
            _hashCode += getWork_time_spent().hashCode();
        }
        if (getWork_type() != null) {
            _hashCode += getWork_type().hashCode();
        }
        if (getWork_type_code() != null) {
            _hashCode += getWork_type_code().hashCode();
        }
        if (getWork_view_type() != null) {
            _hashCode += getWork_view_type().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Incident.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Incident"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("affected_ci_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "affected_ci_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("affected_ci_identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "affected_ci_identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("affected_ci_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "affected_ci_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "approvalList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assigned_contact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "assigned_contact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assigned_group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "assigned_group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assigned_to_group_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "assigned_to_group_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assigned_to_individual_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "assigned_to_individual_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("case_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "case_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cause");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "cause"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccti_category");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ccti_category"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccti_class");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ccti_class"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccti_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ccti_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccti_item");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ccti_item"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ccti_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ccti_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("created_by_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "created_by_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("created_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "created_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custom_attributes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "custom_attributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "CustomAttribute"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description_long");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "description_long"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("global_ticket_item_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "global_ticket_item_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("last_closed_by_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "last_closed_by_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("last_closed_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "last_closed_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("last_used_action_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "last_used_action_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modified_by_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "modified_by_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modified_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "modified_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parent_row_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "parent_row_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parent_ticket_identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "parent_ticket_identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_address_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_address_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_alt_email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_alt_email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_alt_phone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_alt_phone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_contact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_contact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_lvl1_org_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_lvl1_org_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_lvl2_org_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_lvl2_org_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_lvl3_org_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_lvl3_org_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person1_org_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person1_org_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_address_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_address_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_alt_email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_alt_email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_alt_phone");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_alt_phone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_contact_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_contact_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_lvl1_org_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_lvl1_org_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_lvl2_org_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_lvl2_org_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_lvl3_org_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_lvl3_org_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person2_org_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "person2_org_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("related_to_global_issue_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "related_to_global_issue_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requested_for_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "requested_for_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requester_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "requester_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resolution");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "resolution"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("row_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "row_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("severity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "severity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sla_holiday_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "sla_holiday_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sla_holiday_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "sla_holiday_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sla_start_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "sla_start_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sla_tz_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "sla_tz_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sla_tz_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "sla_tz_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solution_used_from_item_case");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "solution_used_from_item_case"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("solution_used_from_item_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "solution_used_from_item_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("support_email_address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "support_email_address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("this_ticket_is");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "this_ticket_is"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_impact");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_impact"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_impact_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_impact_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_phase");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_phase"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_priority");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_priority_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_priority_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_reason_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_reason_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_solution_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_solution_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_source");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_source"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_source_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_source_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_urgency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_urgency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_urgency_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_urgency_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("total_worklog_time_spent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "total_worklog_time_spent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vip_flag_person1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "vip_flag_person1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vip_flag_person2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "vip_flag_person2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_actual_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_actual_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_time_spent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_time_spent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_type_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_type_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("work_view_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "work_view_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
