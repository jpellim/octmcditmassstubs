/**
 * Communication.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.beans.xsd;

public class Communication  extends com.inteqnet.components.webservice.beans.xsd.Bean  implements java.io.Serializable {
    private java.lang.String comm_body;

    private java.lang.String comm_compose_message_on_send;

    private java.lang.String comm_created_by;

    private java.lang.Integer comm_created_by_id;

    private java.lang.String comm_created_date;

    private java.lang.Integer comm_delivery_status;

    private java.lang.String comm_reply_to;

    private java.lang.String comm_reply_to_name;

    private java.lang.String comm_sendbcc;

    private java.lang.String comm_sendcc;

    private java.lang.String comm_sendto;

    private java.lang.String comm_subject;

    private java.lang.Integer comm_template_id;

    private java.lang.String comm_trace_log;

    private java.lang.String comm_type;

    private java.lang.Integer comm_type_mask;

    private java.lang.Integer fb_form_notification_id;

    private java.lang.Integer item_id;

    private java.lang.Integer row_id;

    private java.lang.Integer sched_task_id;

    private java.lang.String ticket_identifier;

    public Communication() {
    }

    public Communication(
           java.lang.String comm_body,
           java.lang.String comm_compose_message_on_send,
           java.lang.String comm_created_by,
           java.lang.Integer comm_created_by_id,
           java.lang.String comm_created_date,
           java.lang.Integer comm_delivery_status,
           java.lang.String comm_reply_to,
           java.lang.String comm_reply_to_name,
           java.lang.String comm_sendbcc,
           java.lang.String comm_sendcc,
           java.lang.String comm_sendto,
           java.lang.String comm_subject,
           java.lang.Integer comm_template_id,
           java.lang.String comm_trace_log,
           java.lang.String comm_type,
           java.lang.Integer comm_type_mask,
           java.lang.Integer fb_form_notification_id,
           java.lang.Integer item_id,
           java.lang.Integer row_id,
           java.lang.Integer sched_task_id,
           java.lang.String ticket_identifier) {
        this.comm_body = comm_body;
        this.comm_compose_message_on_send = comm_compose_message_on_send;
        this.comm_created_by = comm_created_by;
        this.comm_created_by_id = comm_created_by_id;
        this.comm_created_date = comm_created_date;
        this.comm_delivery_status = comm_delivery_status;
        this.comm_reply_to = comm_reply_to;
        this.comm_reply_to_name = comm_reply_to_name;
        this.comm_sendbcc = comm_sendbcc;
        this.comm_sendcc = comm_sendcc;
        this.comm_sendto = comm_sendto;
        this.comm_subject = comm_subject;
        this.comm_template_id = comm_template_id;
        this.comm_trace_log = comm_trace_log;
        this.comm_type = comm_type;
        this.comm_type_mask = comm_type_mask;
        this.fb_form_notification_id = fb_form_notification_id;
        this.item_id = item_id;
        this.row_id = row_id;
        this.sched_task_id = sched_task_id;
        this.ticket_identifier = ticket_identifier;
    }


    /**
     * Gets the comm_body value for this Communication.
     * 
     * @return comm_body
     */
    public java.lang.String getComm_body() {
        return comm_body;
    }


    /**
     * Sets the comm_body value for this Communication.
     * 
     * @param comm_body
     */
    public void setComm_body(java.lang.String comm_body) {
        this.comm_body = comm_body;
    }


    /**
     * Gets the comm_compose_message_on_send value for this Communication.
     * 
     * @return comm_compose_message_on_send
     */
    public java.lang.String getComm_compose_message_on_send() {
        return comm_compose_message_on_send;
    }


    /**
     * Sets the comm_compose_message_on_send value for this Communication.
     * 
     * @param comm_compose_message_on_send
     */
    public void setComm_compose_message_on_send(java.lang.String comm_compose_message_on_send) {
        this.comm_compose_message_on_send = comm_compose_message_on_send;
    }


    /**
     * Gets the comm_created_by value for this Communication.
     * 
     * @return comm_created_by
     */
    public java.lang.String getComm_created_by() {
        return comm_created_by;
    }


    /**
     * Sets the comm_created_by value for this Communication.
     * 
     * @param comm_created_by
     */
    public void setComm_created_by(java.lang.String comm_created_by) {
        this.comm_created_by = comm_created_by;
    }


    /**
     * Gets the comm_created_by_id value for this Communication.
     * 
     * @return comm_created_by_id
     */
    public java.lang.Integer getComm_created_by_id() {
        return comm_created_by_id;
    }


    /**
     * Sets the comm_created_by_id value for this Communication.
     * 
     * @param comm_created_by_id
     */
    public void setComm_created_by_id(java.lang.Integer comm_created_by_id) {
        this.comm_created_by_id = comm_created_by_id;
    }


    /**
     * Gets the comm_created_date value for this Communication.
     * 
     * @return comm_created_date
     */
    public java.lang.String getComm_created_date() {
        return comm_created_date;
    }


    /**
     * Sets the comm_created_date value for this Communication.
     * 
     * @param comm_created_date
     */
    public void setComm_created_date(java.lang.String comm_created_date) {
        this.comm_created_date = comm_created_date;
    }


    /**
     * Gets the comm_delivery_status value for this Communication.
     * 
     * @return comm_delivery_status
     */
    public java.lang.Integer getComm_delivery_status() {
        return comm_delivery_status;
    }


    /**
     * Sets the comm_delivery_status value for this Communication.
     * 
     * @param comm_delivery_status
     */
    public void setComm_delivery_status(java.lang.Integer comm_delivery_status) {
        this.comm_delivery_status = comm_delivery_status;
    }


    /**
     * Gets the comm_reply_to value for this Communication.
     * 
     * @return comm_reply_to
     */
    public java.lang.String getComm_reply_to() {
        return comm_reply_to;
    }


    /**
     * Sets the comm_reply_to value for this Communication.
     * 
     * @param comm_reply_to
     */
    public void setComm_reply_to(java.lang.String comm_reply_to) {
        this.comm_reply_to = comm_reply_to;
    }


    /**
     * Gets the comm_reply_to_name value for this Communication.
     * 
     * @return comm_reply_to_name
     */
    public java.lang.String getComm_reply_to_name() {
        return comm_reply_to_name;
    }


    /**
     * Sets the comm_reply_to_name value for this Communication.
     * 
     * @param comm_reply_to_name
     */
    public void setComm_reply_to_name(java.lang.String comm_reply_to_name) {
        this.comm_reply_to_name = comm_reply_to_name;
    }


    /**
     * Gets the comm_sendbcc value for this Communication.
     * 
     * @return comm_sendbcc
     */
    public java.lang.String getComm_sendbcc() {
        return comm_sendbcc;
    }


    /**
     * Sets the comm_sendbcc value for this Communication.
     * 
     * @param comm_sendbcc
     */
    public void setComm_sendbcc(java.lang.String comm_sendbcc) {
        this.comm_sendbcc = comm_sendbcc;
    }


    /**
     * Gets the comm_sendcc value for this Communication.
     * 
     * @return comm_sendcc
     */
    public java.lang.String getComm_sendcc() {
        return comm_sendcc;
    }


    /**
     * Sets the comm_sendcc value for this Communication.
     * 
     * @param comm_sendcc
     */
    public void setComm_sendcc(java.lang.String comm_sendcc) {
        this.comm_sendcc = comm_sendcc;
    }


    /**
     * Gets the comm_sendto value for this Communication.
     * 
     * @return comm_sendto
     */
    public java.lang.String getComm_sendto() {
        return comm_sendto;
    }


    /**
     * Sets the comm_sendto value for this Communication.
     * 
     * @param comm_sendto
     */
    public void setComm_sendto(java.lang.String comm_sendto) {
        this.comm_sendto = comm_sendto;
    }


    /**
     * Gets the comm_subject value for this Communication.
     * 
     * @return comm_subject
     */
    public java.lang.String getComm_subject() {
        return comm_subject;
    }


    /**
     * Sets the comm_subject value for this Communication.
     * 
     * @param comm_subject
     */
    public void setComm_subject(java.lang.String comm_subject) {
        this.comm_subject = comm_subject;
    }


    /**
     * Gets the comm_template_id value for this Communication.
     * 
     * @return comm_template_id
     */
    public java.lang.Integer getComm_template_id() {
        return comm_template_id;
    }


    /**
     * Sets the comm_template_id value for this Communication.
     * 
     * @param comm_template_id
     */
    public void setComm_template_id(java.lang.Integer comm_template_id) {
        this.comm_template_id = comm_template_id;
    }


    /**
     * Gets the comm_trace_log value for this Communication.
     * 
     * @return comm_trace_log
     */
    public java.lang.String getComm_trace_log() {
        return comm_trace_log;
    }


    /**
     * Sets the comm_trace_log value for this Communication.
     * 
     * @param comm_trace_log
     */
    public void setComm_trace_log(java.lang.String comm_trace_log) {
        this.comm_trace_log = comm_trace_log;
    }


    /**
     * Gets the comm_type value for this Communication.
     * 
     * @return comm_type
     */
    public java.lang.String getComm_type() {
        return comm_type;
    }


    /**
     * Sets the comm_type value for this Communication.
     * 
     * @param comm_type
     */
    public void setComm_type(java.lang.String comm_type) {
        this.comm_type = comm_type;
    }


    /**
     * Gets the comm_type_mask value for this Communication.
     * 
     * @return comm_type_mask
     */
    public java.lang.Integer getComm_type_mask() {
        return comm_type_mask;
    }


    /**
     * Sets the comm_type_mask value for this Communication.
     * 
     * @param comm_type_mask
     */
    public void setComm_type_mask(java.lang.Integer comm_type_mask) {
        this.comm_type_mask = comm_type_mask;
    }


    /**
     * Gets the fb_form_notification_id value for this Communication.
     * 
     * @return fb_form_notification_id
     */
    public java.lang.Integer getFb_form_notification_id() {
        return fb_form_notification_id;
    }


    /**
     * Sets the fb_form_notification_id value for this Communication.
     * 
     * @param fb_form_notification_id
     */
    public void setFb_form_notification_id(java.lang.Integer fb_form_notification_id) {
        this.fb_form_notification_id = fb_form_notification_id;
    }


    /**
     * Gets the item_id value for this Communication.
     * 
     * @return item_id
     */
    public java.lang.Integer getItem_id() {
        return item_id;
    }


    /**
     * Sets the item_id value for this Communication.
     * 
     * @param item_id
     */
    public void setItem_id(java.lang.Integer item_id) {
        this.item_id = item_id;
    }


    /**
     * Gets the row_id value for this Communication.
     * 
     * @return row_id
     */
    public java.lang.Integer getRow_id() {
        return row_id;
    }


    /**
     * Sets the row_id value for this Communication.
     * 
     * @param row_id
     */
    public void setRow_id(java.lang.Integer row_id) {
        this.row_id = row_id;
    }


    /**
     * Gets the sched_task_id value for this Communication.
     * 
     * @return sched_task_id
     */
    public java.lang.Integer getSched_task_id() {
        return sched_task_id;
    }


    /**
     * Sets the sched_task_id value for this Communication.
     * 
     * @param sched_task_id
     */
    public void setSched_task_id(java.lang.Integer sched_task_id) {
        this.sched_task_id = sched_task_id;
    }


    /**
     * Gets the ticket_identifier value for this Communication.
     * 
     * @return ticket_identifier
     */
    public java.lang.String getTicket_identifier() {
        return ticket_identifier;
    }


    /**
     * Sets the ticket_identifier value for this Communication.
     * 
     * @param ticket_identifier
     */
    public void setTicket_identifier(java.lang.String ticket_identifier) {
        this.ticket_identifier = ticket_identifier;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Communication)) return false;
        Communication other = (Communication) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.comm_body==null && other.getComm_body()==null) || 
             (this.comm_body!=null &&
              this.comm_body.equals(other.getComm_body()))) &&
            ((this.comm_compose_message_on_send==null && other.getComm_compose_message_on_send()==null) || 
             (this.comm_compose_message_on_send!=null &&
              this.comm_compose_message_on_send.equals(other.getComm_compose_message_on_send()))) &&
            ((this.comm_created_by==null && other.getComm_created_by()==null) || 
             (this.comm_created_by!=null &&
              this.comm_created_by.equals(other.getComm_created_by()))) &&
            ((this.comm_created_by_id==null && other.getComm_created_by_id()==null) || 
             (this.comm_created_by_id!=null &&
              this.comm_created_by_id.equals(other.getComm_created_by_id()))) &&
            ((this.comm_created_date==null && other.getComm_created_date()==null) || 
             (this.comm_created_date!=null &&
              this.comm_created_date.equals(other.getComm_created_date()))) &&
            ((this.comm_delivery_status==null && other.getComm_delivery_status()==null) || 
             (this.comm_delivery_status!=null &&
              this.comm_delivery_status.equals(other.getComm_delivery_status()))) &&
            ((this.comm_reply_to==null && other.getComm_reply_to()==null) || 
             (this.comm_reply_to!=null &&
              this.comm_reply_to.equals(other.getComm_reply_to()))) &&
            ((this.comm_reply_to_name==null && other.getComm_reply_to_name()==null) || 
             (this.comm_reply_to_name!=null &&
              this.comm_reply_to_name.equals(other.getComm_reply_to_name()))) &&
            ((this.comm_sendbcc==null && other.getComm_sendbcc()==null) || 
             (this.comm_sendbcc!=null &&
              this.comm_sendbcc.equals(other.getComm_sendbcc()))) &&
            ((this.comm_sendcc==null && other.getComm_sendcc()==null) || 
             (this.comm_sendcc!=null &&
              this.comm_sendcc.equals(other.getComm_sendcc()))) &&
            ((this.comm_sendto==null && other.getComm_sendto()==null) || 
             (this.comm_sendto!=null &&
              this.comm_sendto.equals(other.getComm_sendto()))) &&
            ((this.comm_subject==null && other.getComm_subject()==null) || 
             (this.comm_subject!=null &&
              this.comm_subject.equals(other.getComm_subject()))) &&
            ((this.comm_template_id==null && other.getComm_template_id()==null) || 
             (this.comm_template_id!=null &&
              this.comm_template_id.equals(other.getComm_template_id()))) &&
            ((this.comm_trace_log==null && other.getComm_trace_log()==null) || 
             (this.comm_trace_log!=null &&
              this.comm_trace_log.equals(other.getComm_trace_log()))) &&
            ((this.comm_type==null && other.getComm_type()==null) || 
             (this.comm_type!=null &&
              this.comm_type.equals(other.getComm_type()))) &&
            ((this.comm_type_mask==null && other.getComm_type_mask()==null) || 
             (this.comm_type_mask!=null &&
              this.comm_type_mask.equals(other.getComm_type_mask()))) &&
            ((this.fb_form_notification_id==null && other.getFb_form_notification_id()==null) || 
             (this.fb_form_notification_id!=null &&
              this.fb_form_notification_id.equals(other.getFb_form_notification_id()))) &&
            ((this.item_id==null && other.getItem_id()==null) || 
             (this.item_id!=null &&
              this.item_id.equals(other.getItem_id()))) &&
            ((this.row_id==null && other.getRow_id()==null) || 
             (this.row_id!=null &&
              this.row_id.equals(other.getRow_id()))) &&
            ((this.sched_task_id==null && other.getSched_task_id()==null) || 
             (this.sched_task_id!=null &&
              this.sched_task_id.equals(other.getSched_task_id()))) &&
            ((this.ticket_identifier==null && other.getTicket_identifier()==null) || 
             (this.ticket_identifier!=null &&
              this.ticket_identifier.equals(other.getTicket_identifier())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getComm_body() != null) {
            _hashCode += getComm_body().hashCode();
        }
        if (getComm_compose_message_on_send() != null) {
            _hashCode += getComm_compose_message_on_send().hashCode();
        }
        if (getComm_created_by() != null) {
            _hashCode += getComm_created_by().hashCode();
        }
        if (getComm_created_by_id() != null) {
            _hashCode += getComm_created_by_id().hashCode();
        }
        if (getComm_created_date() != null) {
            _hashCode += getComm_created_date().hashCode();
        }
        if (getComm_delivery_status() != null) {
            _hashCode += getComm_delivery_status().hashCode();
        }
        if (getComm_reply_to() != null) {
            _hashCode += getComm_reply_to().hashCode();
        }
        if (getComm_reply_to_name() != null) {
            _hashCode += getComm_reply_to_name().hashCode();
        }
        if (getComm_sendbcc() != null) {
            _hashCode += getComm_sendbcc().hashCode();
        }
        if (getComm_sendcc() != null) {
            _hashCode += getComm_sendcc().hashCode();
        }
        if (getComm_sendto() != null) {
            _hashCode += getComm_sendto().hashCode();
        }
        if (getComm_subject() != null) {
            _hashCode += getComm_subject().hashCode();
        }
        if (getComm_template_id() != null) {
            _hashCode += getComm_template_id().hashCode();
        }
        if (getComm_trace_log() != null) {
            _hashCode += getComm_trace_log().hashCode();
        }
        if (getComm_type() != null) {
            _hashCode += getComm_type().hashCode();
        }
        if (getComm_type_mask() != null) {
            _hashCode += getComm_type_mask().hashCode();
        }
        if (getFb_form_notification_id() != null) {
            _hashCode += getFb_form_notification_id().hashCode();
        }
        if (getItem_id() != null) {
            _hashCode += getItem_id().hashCode();
        }
        if (getRow_id() != null) {
            _hashCode += getRow_id().hashCode();
        }
        if (getSched_task_id() != null) {
            _hashCode += getSched_task_id().hashCode();
        }
        if (getTicket_identifier() != null) {
            _hashCode += getTicket_identifier().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Communication.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Communication"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_body");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_body"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_compose_message_on_send");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_compose_message_on_send"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_created_by");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_created_by"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_created_by_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_created_by_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_created_date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_created_date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_delivery_status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_delivery_status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_reply_to");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_reply_to"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_reply_to_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_reply_to_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_sendbcc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_sendbcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_sendcc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_sendcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_sendto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_sendto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_subject");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_subject"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_template_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_template_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_trace_log");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_trace_log"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comm_type_mask");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "comm_type_mask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fb_form_notification_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "fb_form_notification_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("item_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "item_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("row_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "row_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sched_task_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "sched_task_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticket_identifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ticket_identifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
