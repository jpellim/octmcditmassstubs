/**
 * CustomAttribute.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.beans.xsd;

public class CustomAttribute  extends com.inteqnet.components.webservice.beans.xsd.Bean  implements java.io.Serializable {
    private java.lang.String attribute_name;

    private java.lang.String attribute_value;

    public CustomAttribute() {
    }

    public CustomAttribute(
           java.lang.String attribute_name,
           java.lang.String attribute_value) {
        this.attribute_name = attribute_name;
        this.attribute_value = attribute_value;
    }


    /**
     * Gets the attribute_name value for this CustomAttribute.
     * 
     * @return attribute_name
     */
    public java.lang.String getAttribute_name() {
        return attribute_name;
    }


    /**
     * Sets the attribute_name value for this CustomAttribute.
     * 
     * @param attribute_name
     */
    public void setAttribute_name(java.lang.String attribute_name) {
        this.attribute_name = attribute_name;
    }


    /**
     * Gets the attribute_value value for this CustomAttribute.
     * 
     * @return attribute_value
     */
    public java.lang.String getAttribute_value() {
        return attribute_value;
    }


    /**
     * Sets the attribute_value value for this CustomAttribute.
     * 
     * @param attribute_value
     */
    public void setAttribute_value(java.lang.String attribute_value) {
        this.attribute_value = attribute_value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomAttribute)) return false;
        CustomAttribute other = (CustomAttribute) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.attribute_name==null && other.getAttribute_name()==null) || 
             (this.attribute_name!=null &&
              this.attribute_name.equals(other.getAttribute_name()))) &&
            ((this.attribute_value==null && other.getAttribute_value()==null) || 
             (this.attribute_value!=null &&
              this.attribute_value.equals(other.getAttribute_value())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAttribute_name() != null) {
            _hashCode += getAttribute_name().hashCode();
        }
        if (getAttribute_value() != null) {
            _hashCode += getAttribute_value().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomAttribute.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "CustomAttribute"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attribute_name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "attribute_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attribute_value");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "attribute_value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
