/**
 * ExtendedSettings.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.beans.xsd;

public class ExtendedSettings  implements java.io.Serializable {
    private java.lang.String responseFormat;

    public ExtendedSettings() {
    }

    public ExtendedSettings(
           java.lang.String responseFormat) {
           this.responseFormat = responseFormat;
    }


    /**
     * Gets the responseFormat value for this ExtendedSettings.
     * 
     * @return responseFormat
     */
    public java.lang.String getResponseFormat() {
        return responseFormat;
    }


    /**
     * Sets the responseFormat value for this ExtendedSettings.
     * 
     * @param responseFormat
     */
    public void setResponseFormat(java.lang.String responseFormat) {
        this.responseFormat = responseFormat;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExtendedSettings)) return false;
        ExtendedSettings other = (ExtendedSettings) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.responseFormat==null && other.getResponseFormat()==null) || 
             (this.responseFormat!=null &&
              this.responseFormat.equals(other.getResponseFormat())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResponseFormat() != null) {
            _hashCode += getResponseFormat().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExtendedSettings.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "responseFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
