/**
 * IncidentSoap11BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.wrappers;

import java.rmi.RemoteException;

import com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse;
import com.inteqnet.core.appservices.webservice.beans.xsd.Credentials;
import com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings;

public class IncidentSoap11BindingStub extends org.apache.axis.client.Stub implements com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[21];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listRelatedTickets");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getIncident");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendCommunication");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "commBean"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Communication"), com.inteqnet.core.appservices.webservice.beans.xsd.Communication.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listActivities");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "searchText"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("relateAttachment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "attachmentId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listRelatedConfigurationItems");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listWorklogs");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "searchText"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("unrelateConfigurationItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "configurationItemId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "configurationItemIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("unrelateTicket");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "parentTicketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "relateTicketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("relateTicket");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "parentTicketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "relateTicketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("relateConfigurationItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "configurationItemId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "configurationItemIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listIncidents");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "searchText"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("reportIncident");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "incBean"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Incident"), com.inteqnet.core.appservices.webservice.beans.xsd.Incident.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listAttachments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateIncident");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "incBean"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Incident"), com.inteqnet.core.appservices.webservice.beans.xsd.Incident.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("addWorklog");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "workglogBean"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Worklog"), com.inteqnet.core.appservices.webservice.beans.xsd.Worklog.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateWorklog");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "workglogBean"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Worklog"), com.inteqnet.core.appservices.webservice.beans.xsd.Worklog.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listSLAComplianceLevels");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "searchText"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("listCommunications");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "searchText"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("unrelateAttachment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "attachmentId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateAttachment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "credentials"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials"), com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "extendedSettings"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings"), com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "ticketIdentifier"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "attachmentId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "attach_is_private"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse"));
        oper.setReturnClass(com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

    }

    public IncidentSoap11BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public IncidentSoap11BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public IncidentSoap11BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Communication");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.core.appservices.webservice.beans.xsd.Communication.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Credentials");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.core.appservices.webservice.beans.xsd.Credentials.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "CustomAttribute");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.core.appservices.webservice.beans.xsd.CustomAttribute.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "ExtendedSettings");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Incident");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.core.appservices.webservice.beans.xsd.Incident.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://beans.webservice.appservices.core.inteqnet.com/xsd", "Worklog");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.core.appservices.webservice.beans.xsd.Worklog.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "Bean");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.components.webservice.beans.xsd.Bean.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://beans.webservice.components.inteqnet.com/xsd", "DefaultServiceResponse");
            cachedSerQNames.add(qName);
            cls = com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listRelatedTickets(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listRelatedTickets");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listRelatedTickets"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse getIncident(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:getIncident");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "getIncident"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse sendCommunication(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Communication commBean) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:sendCommunication");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "sendCommunication"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, commBean});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listActivities(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listActivities");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listActivities"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, searchText});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse relateAttachment(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, int attachmentId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:relateAttachment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "relateAttachment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, new java.lang.Integer(attachmentId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listRelatedConfigurationItems(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listRelatedConfigurationItems");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listRelatedConfigurationItems"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listWorklogs(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listWorklogs");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listWorklogs"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, searchText});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse unrelateConfigurationItem(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, int configurationItemId, java.lang.String configurationItemIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:unrelateConfigurationItem");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "unrelateConfigurationItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, new java.lang.Integer(configurationItemId), configurationItemIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse unrelateTicket(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String parentTicketIdentifier, java.lang.String relateTicketIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:unrelateTicket");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "unrelateTicket"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, parentTicketIdentifier, relateTicketIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse relateTicket(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String parentTicketIdentifier, java.lang.String relateTicketIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:relateTicket");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "relateTicket"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, parentTicketIdentifier, relateTicketIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse relateConfigurationItem(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, int configurationItemId, java.lang.String configurationItemIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:relateConfigurationItem");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "relateConfigurationItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, new java.lang.Integer(configurationItemId), configurationItemIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listIncidents(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String searchText) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listIncidents");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listIncidents"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, searchText});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse reportIncident(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Incident incBean) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:reportIncident");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "reportIncident"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, incBean});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listAttachments(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listAttachments");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listAttachments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse updateIncident(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Incident incBean) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:updateIncident");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "updateIncident"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, incBean});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse addWorklog(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Worklog workglogBean) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:addWorklog");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "addWorklog"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, workglogBean});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse updateWorklog(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Worklog workglogBean) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:updateWorklog");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "updateWorklog"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, workglogBean});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listSLAComplianceLevels(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listSLAComplianceLevels");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listSLAComplianceLevels"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, searchText});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listCommunications(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:listCommunications");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "listCommunications"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, searchText});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse unrelateAttachment(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, int attachmentId) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:unrelateAttachment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "unrelateAttachment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, new java.lang.Integer(attachmentId)});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse updateAttachment(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, int attachmentId, java.lang.String attach_is_private) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:updateAttachment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "updateAttachment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, new java.lang.Integer(attachmentId), attach_is_private});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

	@Override
	public DefaultServiceResponse unrelateConfigurationItem(
			Credentials credentials, ExtendedSettings extendedSettings,
			String ticketIdentifier, Integer configurationItemId,
			String configurationItemIdentifier) throws RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:unrelateConfigurationItem");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "unrelateConfigurationItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, configurationItemId, configurationItemIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
	}

	@Override
	public DefaultServiceResponse relateAttachment(Credentials credentials,
			ExtendedSettings extendedSettings, String ticketIdentifier,
			Integer attachmentId) throws RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:relateAttachment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "relateAttachment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, attachmentId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
	}

	@Override
	public DefaultServiceResponse unrelateAttachment(Credentials credentials,
			ExtendedSettings extendedSettings, String ticketIdentifier,
			Integer attachmentId) throws RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:unrelateAttachment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "unrelateAttachment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, attachmentId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
  }
}

	@Override
	public DefaultServiceResponse relateConfigurationItem(
			Credentials credentials, ExtendedSettings extendedSettings,
			String ticketIdentifier, Integer configurationItemId,
			String configurationItemIdentifier) throws RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:relateConfigurationItem");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "relateConfigurationItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, configurationItemId, configurationItemIdentifier});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
  }
}

	@Override
	public DefaultServiceResponse updateAttachment(Credentials credentials,
			ExtendedSettings extendedSettings, String ticketIdentifier,
			Integer attachmentId, String attach_is_private)
			throws RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:updateAttachment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "updateAttachment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {credentials, extendedSettings, ticketIdentifier, attachmentId, attach_is_private});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
  }
}

}
