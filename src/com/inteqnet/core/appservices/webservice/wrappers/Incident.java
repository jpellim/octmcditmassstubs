/**
 * Incident.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.wrappers;

public interface Incident extends javax.xml.rpc.Service {
    public java.lang.String getIncidentHttpSoap11EndpointAddress();

    public com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType getIncidentHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType getIncidentHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
