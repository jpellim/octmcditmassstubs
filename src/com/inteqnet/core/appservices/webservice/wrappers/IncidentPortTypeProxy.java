package com.inteqnet.core.appservices.webservice.wrappers;

public class IncidentPortTypeProxy implements com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType {
  private String _endpoint = null;
  private com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType incidentPortType = null;
  
  public IncidentPortTypeProxy() {
    _initIncidentPortTypeProxy();
  }
  
  public IncidentPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initIncidentPortTypeProxy();
  }
  
  private void _initIncidentPortTypeProxy() {
    try {
      incidentPortType = (new com.inteqnet.core.appservices.webservice.wrappers.IncidentLocator()).getIncidentHttpSoap11Endpoint();
      if (incidentPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)incidentPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)incidentPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (incidentPortType != null)
      ((javax.xml.rpc.Stub)incidentPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType getIncidentPortType() {
    if (incidentPortType == null)
      _initIncidentPortTypeProxy();
    return incidentPortType;
  }

  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listRelatedTickets(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listRelatedTickets(credentials, extendedSettings, ticketIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listWorklogs(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listWorklogs(credentials, extendedSettings, ticketIdentifier, searchText);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listAttachments(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listAttachments(credentials, extendedSettings, ticketIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse getIncident(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.getIncident(credentials, extendedSettings, ticketIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse unrelateConfigurationItem(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.Integer configurationItemId, java.lang.String configurationItemIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.unrelateConfigurationItem(credentials, extendedSettings, ticketIdentifier, configurationItemId, configurationItemIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse sendCommunication(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Communication commBean) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.sendCommunication(credentials, extendedSettings, commBean);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listCommunications(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listCommunications(credentials, extendedSettings, ticketIdentifier, searchText);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse updateIncident(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Incident incBean) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.updateIncident(credentials, extendedSettings, incBean);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse unrelateTicket(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String parentTicketIdentifier, java.lang.String relateTicketIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.unrelateTicket(credentials, extendedSettings, parentTicketIdentifier, relateTicketIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse addWorklog(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Worklog workglogBean) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.addWorklog(credentials, extendedSettings, workglogBean);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse relateAttachment(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.Integer attachmentId) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.relateAttachment(credentials, extendedSettings, ticketIdentifier, attachmentId);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse unrelateAttachment(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.Integer attachmentId) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.unrelateAttachment(credentials, extendedSettings, ticketIdentifier, attachmentId);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse updateWorklog(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Worklog workglogBean) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.updateWorklog(credentials, extendedSettings, workglogBean);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listSLAComplianceLevels(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listSLAComplianceLevels(credentials, extendedSettings, ticketIdentifier, searchText);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse relateConfigurationItem(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.Integer configurationItemId, java.lang.String configurationItemIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.relateConfigurationItem(credentials, extendedSettings, ticketIdentifier, configurationItemId, configurationItemIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse updateAttachment(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.Integer attachmentId, java.lang.String attach_is_private) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.updateAttachment(credentials, extendedSettings, ticketIdentifier, attachmentId, attach_is_private);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listRelatedConfigurationItems(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listRelatedConfigurationItems(credentials, extendedSettings, ticketIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse relateTicket(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String parentTicketIdentifier, java.lang.String relateTicketIdentifier) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.relateTicket(credentials, extendedSettings, parentTicketIdentifier, relateTicketIdentifier);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listActivities(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String ticketIdentifier, java.lang.String searchText) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listActivities(credentials, extendedSettings, ticketIdentifier, searchText);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse listIncidents(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, java.lang.String searchText) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.listIncidents(credentials, extendedSettings, searchText);
	  }
	  
	  public com.inteqnet.components.webservice.beans.xsd.DefaultServiceResponse reportIncident(com.inteqnet.core.appservices.webservice.beans.xsd.Credentials credentials, com.inteqnet.core.appservices.webservice.beans.xsd.ExtendedSettings extendedSettings, com.inteqnet.core.appservices.webservice.beans.xsd.Incident incBean) throws java.rmi.RemoteException{
	    if (incidentPortType == null)
	      _initIncidentPortTypeProxy();
	    return incidentPortType.reportIncident(credentials, extendedSettings, incBean);
	  }
  
}