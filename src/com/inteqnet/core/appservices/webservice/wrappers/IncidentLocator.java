/**
 * IncidentLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.inteqnet.core.appservices.webservice.wrappers;

public class IncidentLocator extends org.apache.axis.client.Service implements com.inteqnet.core.appservices.webservice.wrappers.Incident {

    public IncidentLocator() {
    }


    public IncidentLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IncidentLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for IncidentHttpSoap11Endpoint
    //private java.lang.String IncidentHttpSoap11Endpoint_address = "https://sm2.saas.ca.com/NimsoftServiceDesk/servicedesk/webservices/Incident.IncidentHttpSoap11Endpoint/";
    //private java.lang.String IncidentHttpSoap11Endpoint_address = "https://csm2.serviceaide.com/NimsoftServiceDesk/servicedesk/webservices/Incident.IncidentHttpSoap11Endpoint/";
    private java.lang.String IncidentHttpSoap11Endpoint_address = "https://direct-csm2.serviceaide.com/NimsoftServiceDesk/servicedesk/webservices/Incident.IncidentHttpSoap11Endpoint/";
    
    public java.lang.String getIncidentHttpSoap11EndpointAddress() {
        return IncidentHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String IncidentHttpSoap11EndpointWSDDServiceName = "IncidentHttpSoap11Endpoint";

    public java.lang.String getIncidentHttpSoap11EndpointWSDDServiceName() {
        return IncidentHttpSoap11EndpointWSDDServiceName;
    }

    public void setIncidentHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        IncidentHttpSoap11EndpointWSDDServiceName = name;
    }

    public com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType getIncidentHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(IncidentHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getIncidentHttpSoap11Endpoint(endpoint);
    }

    public com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType getIncidentHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.inteqnet.core.appservices.webservice.wrappers.IncidentSoap11BindingStub _stub = new com.inteqnet.core.appservices.webservice.wrappers.IncidentSoap11BindingStub(portAddress, this);
            _stub.setPortName(getIncidentHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setIncidentHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        IncidentHttpSoap11Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.inteqnet.core.appservices.webservice.wrappers.IncidentPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.inteqnet.core.appservices.webservice.wrappers.IncidentSoap11BindingStub _stub = new com.inteqnet.core.appservices.webservice.wrappers.IncidentSoap11BindingStub(new java.net.URL(IncidentHttpSoap11Endpoint_address), this);
                _stub.setPortName(getIncidentHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("IncidentHttpSoap11Endpoint".equals(inputPortName)) {
            return getIncidentHttpSoap11Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "Incident");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://wrappers.webservice.appservices.core.inteqnet.com", "IncidentHttpSoap11Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("IncidentHttpSoap11Endpoint".equals(portName)) {
            setIncidentHttpSoap11EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
